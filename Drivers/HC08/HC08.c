/*
 * HC08.c
 *
 *  Created on: 8 lis 2017
 *      Author: Lukasz
 */

#include "stdbool.h"
#include "stdint.h"
#include "stm32l4xx.h"

#include "Framework/FIFO/FIFO.h"
#include "MCU/Communication/USART1.h"

#define HC08_SingleByte ( 1u )

#define HC08_QueueTimeout ( 3000u )
#define HC08_RxBufferSize ( 120u )
#define HC08_TxBufferSize ( 120u )
#define HC08_SizeOfRxBufferElement ( 1u )
#define HC08_SizeOfTxBufferElement ( 1u )

#define HC08_Write                         USART1_Write
#define HC08_EnableTransmitInterrupt       USART1_EnableTransmitInterrupt
#define HC08_DisableTransmitInterrupt      USART1_DisableTransmitInterrupt
#define HC08_RequestTxInterrupt            USART1_RequestTxInterrupt
#define HC08_IsTxTransmissionInProgress    USART1_IsTxTransmissionInProgress

typedef struct HC08
{
   FIFO_Buffer_T RxBuffer[HC08_RxBufferSize];
   FIFO_Buffer_T TxBuffer[HC08_TxBufferSize];
   FIFO_C RxFIFO;
   FIFO_C TxFIFO;
   bool TxOverwriteOccured;
   bool RxOverwriteOccured;

   bool ReceivedNewData;
} HC08_T;

bool HC08_privWaitForBuffer(uint16_t DataLength);

static HC08_T HC08Driver;

void HC08_Initialize(void)
{
   HC08Driver.RxOverwriteOccured = false;
   HC08Driver.TxOverwriteOccured = false;
   FIFO_Initialize(&HC08Driver.RxFIFO, HC08Driver.RxBuffer, HC08_RxBufferSize, HC08_SizeOfRxBufferElement);
   FIFO_Initialize(&HC08Driver.TxFIFO, HC08Driver.TxBuffer, HC08_TxBufferSize, HC08_SizeOfTxBufferElement);

   HC08Driver.ReceivedNewData = false;
}

void HC08_TransmitCallback(void)
{
   uint8_t Data = 0u;

   if(!FIFO_IsEmpty(&HC08Driver.TxFIFO))
   {
      FIFO_Pop(&HC08Driver.TxFIFO, &Data, 1u);
      HC08_Write(Data);
   }
   else
   {
      HC08_DisableTransmitInterrupt();
   }
}

void HC08_SendBinary(uint8_t Data)
{
   HC08_privWaitForBuffer(HC08_SingleByte);
   HC08_DisableTransmitInterrupt();
   FIFO_Push(&HC08Driver.TxFIFO, &Data, HC08_SingleByte);
   HC08_EnableTransmitInterrupt();

   if(false == HC08_IsTxTransmissionInProgress())
   {
      HC08_RequestTxInterrupt();
   }
}

void HC08_ReceiveCallback(uint8_t Data)
{
   if(!(FIFO_IsSpaceAvailable(&HC08Driver.RxFIFO, HC08_SingleByte)))
   {
      //TODO: How to handle this? Write to buffer? Maybe error code?
      HC08Driver.RxOverwriteOccured = true;
   }

   FIFO_Push(&HC08Driver.RxFIFO, &Data, HC08_SingleByte);
   HC08Driver.ReceivedNewData = true;
}

bool HC08_IsNewDataReceived(void)
{
   return HC08Driver.ReceivedNewData;
}

uint8_t HC08_GetNewData(void)
{
   uint8_t Data;
   if(HC08Driver.ReceivedNewData == true)
   {
      FIFO_Pop(&HC08Driver.RxFIFO, &Data, HC08_SingleByte);
      if(FIFO_IsEmpty(&HC08Driver.RxFIFO) == true)
      {
         HC08Driver.ReceivedNewData = false;
      }
      return Data;
   }
   else
   {
      return '\0';
   }
}

bool HC08_privWaitForBuffer(uint16_t DataLength)
{
   bool TimeoutOccured;

   uint32_t TimeoutCounter = 0u;

   while(!FIFO_IsSpaceAvailable(&HC08Driver.TxFIFO, DataLength))
   {
      TimeoutCounter++;
      if(!HC08_IsTxTransmissionInProgress())
      {
         HC08_RequestTxInterrupt();
      }
      if(TimeoutCounter > HC08_QueueTimeout)
      {
         HC08_DisableTransmitInterrupt();
         //TODO: How to handle this? Write to buffer? Maybe error code?
         HC08Driver.TxOverwriteOccured = true;
         FIFO_Push(&HC08Driver.TxFIFO, "Bluetooth Tx FIFO overwrite occurred!\r", 28u);
         HC08_EnableTransmitInterrupt();
      }
      TimeoutOccured = true;
   }

   return TimeoutOccured;
}

