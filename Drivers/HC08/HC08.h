/*
 * HC08.h
 *
 *  Created on: 8 lis 2017
 *      Author: Lukasz
 */

#ifndef __HC08_H_
#define __HC08_H_

#include "stdbool.h"
#include "stdint.h"

void     HC08_Initialize(void);
void     HC08_SendBinary(uint8_t Data);

bool     HC08_IsNewDataReceived(void);
uint8_t  HC08_GetNewData(void);

#endif /* __HC08_H_ */
