/*
 * ButtonDriverConfig.c
 *
 *  Created on: 16 sie 2017
 *      Author: Lukasz
 */

#include "ButtonDriverConfig.h"

const ButtonDriver_Pin_T Buttons[Button_NumOf] =
{
   [ Button_Left  ] = { .Port = GPIOB, .PinMask = LL_GPIO_PIN_0 },
   [ Button_Right ] = { .Port = GPIOB, .PinMask = LL_GPIO_PIN_1 },
};
