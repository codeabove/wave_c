/*
 * ButtonDriver.c
 *
 *  Created on: 16 sie 2017
 *      Author: Lukasz
 */

#include "stm32l4xx.h"
#include "ButtonDriver.h"
#include "ButtonDriverConfig.h"

#include "MCU/ST/stm32l4xx_ll_gpio.h"

uint32_t Button_GetState(uint8_t ButtonIndex)
{
   return LL_GPIO_IsInputPinSet(Buttons[ButtonIndex].Port, Buttons[ButtonIndex].PinMask);
}
