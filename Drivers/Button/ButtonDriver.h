/*
 * ButtonDriver.h
 *
 *  Created on: 16 sie 2017
 *      Author: Lukasz
 */

#ifndef __ButtonDriver_H_
#define __ButtonDriver_H_

#include "ButtonDriverConfig.h"

uint32_t Button_GetState(uint8_t ButtonIndex);

#endif /* __ButtonDriver_H_ */
