/*
 * ButtonDriverConfig.h
 *
 *  Created on: 16 sie 2017
 *      Author: Lukasz
 */

#ifndef __ButtonDriverConfig_H_
#define __ButtonDriverConfig_H_

#include "MCU/ST/stm32l4xx_ll_gpio.h"

typedef enum Button
{
   Button_Left = 0u,
   Button_Right,
   /**************/
   Button_NumOf
} Button_T;

typedef struct ButtonDriver_Pin
{
   GPIO_TypeDef *Port;
   uint32_t PinMask;
} ButtonDriver_Pin_T;

extern const ButtonDriver_Pin_T Buttons[Button_NumOf];

#endif /* __ButtonDriverConfig_H_ */
