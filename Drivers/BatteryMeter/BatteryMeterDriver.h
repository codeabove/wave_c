/*
 * BatteryMeterDriver.h
 *
 *  Created on: 16 sie 2017
 *      Author: Lukasz
 */

#ifndef __BatteryMeterDriver_H_
#define __BatteryMeterDriver_H_

#include "stm32l4xx.h"

void     BatteryMeter_Perform(void);
void     BatteryMeter_ApplyNewValue(uint16_t NewValue);
uint16_t BatteryMeter_GetBatteryVoltage(void);
float    BatteryMeter_GetBatteryVoltageFloat(void);

#endif /* __BatteryMeterDriver_H_ */
