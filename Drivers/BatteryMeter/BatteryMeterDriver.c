/*
 * BatteryMeterDriver.c
 *
 *  Created on: 16 sie 2017
 *      Author: Lukasz
 */

#include "BatteryMeterDriver.h"

#include "stdbool.h"

#define SamplesAmount                     (     10u )
#define HardwareVoltageDividerCorrector   (      3u )
#define ReferenceVoltage                  (   3300u ) // 3300 * 1mV = 3.3V
#define ReferenceVoltageFloat             (    3.3f )
#define TwelveBitMax                      (   4095u )
#define TwelveBitMaxFloat                 ( 4095.0f )

typedef struct BatteryMeterDriver
{
   volatile uint16_t Samples[SamplesAmount];
   volatile uint8_t NewSampleSlotIndicator;
   volatile bool   NewSamplesApplied;

   uint16_t BatteryVoltage;
   float    BatteryVoltageFloat;
} BatteryMeterDriver_T;

static BatteryMeterDriver_T Driver;

/*!
 * \brief      Function checks if new reading samples are applied
 *             and calculates battery voltage using average dividing
 *
 * @param[in]  void
 * @param[out] void
 */
void BatteryMeter_Perform(void)
{
   uint8_t i;
   uint32_t Sum = 0;
   uint32_t Average;

   if(Driver.NewSamplesApplied == true)
   {
      for(i = 0; i < SamplesAmount; i++)
      {
         Sum += Driver.Samples[i];
      }

      Average = Sum * HardwareVoltageDividerCorrector / SamplesAmount;
      Driver.BatteryVoltage = ReferenceVoltage * Average / TwelveBitMax;
      Driver.BatteryVoltageFloat = ReferenceVoltageFloat * Average / TwelveBitMaxFloat;

      Driver.NewSamplesApplied = false;
   }
}

/*!
 * \brief      Function saves passed value to samples array
 *             and sets new samples flag
 *
 * @param[in]  uint16_t NewValue
 * @param[out] void
 */
void BatteryMeter_ApplyNewValue(uint16_t NewValue)
{
   Driver.Samples[Driver.NewSampleSlotIndicator] = NewValue;

   Driver.NewSampleSlotIndicator++;
   if(Driver.NewSampleSlotIndicator >= SamplesAmount)
   {
      Driver.NewSampleSlotIndicator = 0;
   }

   Driver.NewSamplesApplied = true;
}

/*!
 * \brief      Function returns calculated battery voltage
 *
 * @param[in]  void
 * @param[out] uint16_t - calculated voltage [unit: mV]
 */
uint16_t BatteryMeter_GetBatteryVoltage(void)
{
   return Driver.BatteryVoltage;
}

/*!
 * \brief      Function returns calculated battery voltage
 *
 * @param[in]  void
 * @param[out] float - calculated voltage [unit: V]
 */
float BatteryMeter_GetBatteryVoltageFloat(void)
{
   return Driver.BatteryVoltageFloat;
}


