/*
 * EncoderDriver.c
 *
 *  Created on: 23 wrz 2017
 *      Author: Lukasz
 */

#include "stdbool.h"
#include "stm32l4xx.h"
#include "MCU/ST/stm32l4xx_ll_tim.h"

#include "EncoderDriver.h"

#define EncoderDriver_PerformLoopIntervalTime   ( 100u ) /* [ms] */
#define EncoderDriver_PerformCallsPerSecond     ( 1000u / EncoderDriver_PerformLoopIntervalTime )

#define EncoderDirection_Up   ( LL_TIM_COUNTERDIRECTION_UP )
#define EncoderDirection_Down ( LL_TIM_COUNTERDIRECTION_DOWN )

typedef struct EncoderDriver
{
   const uint32_t PulsesPerRevolution;
   TIM_TypeDef *Timer;
   uint32_t TotalPulses;
   uint32_t CurrentSpeed; // [pps - pulses per second]
   uint32_t LastReading;
} EncoderDriver_T;

static EncoderDriver_T EncoderDriver[Encoder_NumOf] =
{
      [Encoder_Right] = { .Timer = TIM1, .PulsesPerRevolution = 3520u },
      [Encoder_Left ] = { .Timer = TIM8, .PulsesPerRevolution = 3520u },
};

static inline uint32_t EncoderDriver_privGetTimerCounter(TIM_TypeDef *Timer);
static inline uint32_t EncoderDriver_privGetDirection(TIM_TypeDef *Timer);
static inline uint32_t EncoderDriver_privIsCounterOverflowUnderflow(TIM_TypeDef *Timer);
static inline uint32_t EncoderDriver_privGetCounterMax(TIM_TypeDef *Timer);

/*!
 * \brief      Function takes encoder readings and calculates
 *             ticks parameters ex. speed
 *
 * @param[in]  void
 * @param[out] void
 */
void EncoderDriver_Perform( void )
{
   uint8_t iter;
   uint32_t CurrentReading;
   uint32_t CounterDirection;
   int32_t ReadingsDelta;
   int32_t Pulses;
   uint32_t CounterMax;
   bool OverflowUnderflow;

   for(iter = 0; iter < Encoder_NumOf; iter++)
   {
      /* Retrieve all values and states from encoder timer */
      CurrentReading = EncoderDriver_privGetTimerCounter(EncoderDriver[iter].Timer);
      CounterDirection = EncoderDriver_privGetDirection(EncoderDriver[iter].Timer);
      OverflowUnderflow = EncoderDriver_privIsCounterOverflowUnderflow(EncoderDriver[iter].Timer);
      CounterMax = EncoderDriver_privGetCounterMax(EncoderDriver[iter].Timer);

      ReadingsDelta = CurrentReading - EncoderDriver[iter].LastReading;

      /* Calculate pulses counted since last perform loop */
      if(false == OverflowUnderflow)
      {
         Pulses = ReadingsDelta;
      }
      else
      {
         if(EncoderDirection_Up == CounterDirection)
         {
            Pulses = CounterMax + ReadingsDelta;
         }
         else
         {
            Pulses = CounterMax - ReadingsDelta;
         }
      }

      EncoderDriver[iter].TotalPulses += Pulses;

      /* Ticks speed is calculated with derivative over time */
      /* v = dx / dt = Ticks / LoopIntervalTime = Pulses * LoopsPerSecond */
      EncoderDriver[iter].CurrentSpeed = Pulses * EncoderDriver_PerformCallsPerSecond;

      EncoderDriver[iter].LastReading = CurrentReading;
   }
}

/*!
 * \brief      Function returns calculated current speed
 *             of given encoder
 *
 * @param[in]  Encoder_T Encoder - selected encoder
 * @return     uint32_t - current speed
 */
uint32_t EncoderDriver_GetCurrentSpeed( Encoder_T Encoder )
{
   return EncoderDriver[Encoder].CurrentSpeed;
}

/*!
 * \brief      Function returns calculated current speed
 *             of given encoder in RPM units
 *
 * @param[in]  Encoder_T Encoder - selected encoder
 * @return     uint32_t - current speed
 */
uint32_t EncoderDriver_GetCurrentSpeed_RPM( Encoder_T Encoder )
{
   return EncoderDriver[Encoder].CurrentSpeed * 60 / EncoderDriver[Encoder].PulsesPerRevolution;
}


/*!
 * \brief      Wrapper function for getting encoder counter value
 *
 * @param[in]  void
 * @param[out] void
 */
static inline uint32_t EncoderDriver_privGetTimerCounter(TIM_TypeDef *Timer)
{
   return LL_TIM_GetCounter(Timer);
}

/*!
 * \brief      Wrapper function for getting encoder counter direction
 *
 * @param[in]  void
 * @param[out] void
 */
static inline uint32_t EncoderDriver_privGetDirection(TIM_TypeDef *Timer)
{
   return LL_TIM_GetDirection(Timer);
}

/*!
 * \brief      Wrapper function for checking if encoder counter
 *             overflowed or underflowed
 *
 * @param[in]  void
 * @param[out] void
 */
static inline uint32_t EncoderDriver_privIsCounterOverflowUnderflow(TIM_TypeDef *Timer)
{
   uint32_t tmp = LL_TIM_IsActiveFlag_UPDATE(Timer);
   LL_TIM_ClearFlag_UPDATE(Timer);
   return tmp;
}

/*!
 * \brief      Wrapper function for checking encoder timer counter max value
 *
 * @param[in]  void
 * @param[out] void
 */
static inline uint32_t EncoderDriver_privGetCounterMax(TIM_TypeDef *Timer)
{
   return (IS_TIM_32B_COUNTER_INSTANCE(Timer) == true ? 0xFFFFFFFFu : 0xFFFFu);
}
