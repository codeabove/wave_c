/*
 * EncoderDriver.h
 *
 *  Created on: 22 wrz 2017
 *      Author: Lukasz
 */

#ifndef __EncoderDriver_H_
#define __EncoderDriver_H_

typedef enum Encoder
{
   Encoder_Right = 0u,
   Encoder_Left,
   /****************/
   Encoder_NumOf
} Encoder_T;

void EncoderDriver_Perform( void );
uint32_t EncoderDriver_GetCurrentSpeed( Encoder_T Encoder );
uint32_t EncoderDriver_GetCurrentSpeed_RPM( Encoder_T Encoder );

#endif /* __EncoderDriver_H_ */
