/*
 * L3GD20H.c
 *
 *  Created on: 22.04.2017
 *      Author: Lukasz
 */

#include <stdint.h>

#include "MCU/ST/stm32l4xx_ll_utils.h"
#include "MCU/Communication/SPI3.h"
#include "Framework/Logger/Logger.h"
#include "L3GD20H.h"

void L3GD20H_Initialize(void)
{
   /*
    * Startup sequence:
    * 0. 10ms device boot
    * 1. Write CTRL2
    * 2. Write CTRL3
    * 3. Write CTRL4
    * 4. Write CTRL6
    * 5. Write Reference
    * 6. Write IG_THS
    * 7. Write IG_DURATION
    * 8. Write IG_CFG
    * 9. Write CTRL5
    * 10. Write CTRL1
    */
   uint8_t tmp;

   LL_mDelay(20);

   tmp = L3GD20H_POWERMODE_NORMAL | L3GD20H_AXES_ENABLE;
   SPI3_Write(L3GD20H_CTRL1_ADDRESS, &tmp, 1);
}

void L3GD20H_Reboot(void)
{
   uint8_t tmp;

   SPI3_Read(L3GD20H_CTRL5_ADDRESS, &tmp, 1);
   tmp |= L3GD20H_BOOT_REBOOTMEMORY;
   SPI3_Write(L3GD20H_CTRL5_ADDRESS, &tmp, 1);
}

void L3GD20H_SetPowerMode(uint8_t PowerMode)
{
   uint8_t tmp;

   SPI3_Read(L3GD20H_CTRL1_ADDRESS, &tmp, 1);
   tmp &= ~L3GD20H_POWERMODE_NORMAL;
   tmp |= PowerMode;
   SPI3_Write(L3GD20H_CTRL5_ADDRESS, &tmp, 1);
}

uint8_t L3GD20H_GetID(void)
{
   uint8_t tmp;

   SPI3_Read(L3GD20H_WHO_AM_I, &tmp, 1);
   return tmp;
}

uint8_t L3GD20H_GetDataStatus(void)
{
   uint8_t tmp;

   SPI3_Read(L3GD20H_STATUS_ADDRESS, &tmp, 1);
   return tmp;
}

void L3GD20H_GetRawAngleValues(int16_t *OutputBuffer)
{
   uint8_t dataBuffer[6];
   uint8_t tmp;
   uint8_t i;

   //TODO: Add buffer size check

   SPI3_Read(L3GD20H_CTRL4_ADDRESS, &tmp, 1);
   SPI3_Read(L3GD20H_OUT_X_L_ADDRESS, dataBuffer, 6);
//   SPI3_Read(L3GD20H_OUT_X_L_ADDRESS, &dataBuffer[0], 1);
//   SPI3_Read(L3GD20H_OUT_X_H_ADDRESS, &dataBuffer[1], 1);
//   SPI3_Read(L3GD20H_OUT_Y_L_ADDRESS, &dataBuffer[2], 1);
//   SPI3_Read(L3GD20H_OUT_Y_H_ADDRESS, &dataBuffer[3], 1);
//   SPI3_Read(L3GD20H_OUT_Z_L_ADDRESS, &dataBuffer[4], 1);
//   SPI3_Read(L3GD20H_OUT_Z_H_ADDRESS, &dataBuffer[5], 1);

   if((tmp & L3GD20H_BIGLITTLEENDIAN_MSB))
   {
      for(i = 0; i < 3; i++)
      {
         OutputBuffer[i] = (int16_t)(((uint16_t)dataBuffer[2*i] << 8) + dataBuffer[2*i+1]);
      }
   }
   else
   {
      for(i = 0; i < 3; i++)
      {
         OutputBuffer[i] = (int16_t)(((uint16_t)dataBuffer[2*i+1] << 8) + dataBuffer[2*i]);
      }
   }
}

void L3GD20H_EnableInterrupt(uint8_t Interrupt)
{
   uint8_t tmp;

   SPI3_Read(L3GD20H_CTRL3_ADDRESS, &tmp, 1);
   tmp |= Interrupt;
   SPI3_Write(L3GD20H_CTRL3_ADDRESS, &tmp, 1);
}

void L3GD20H_DisableInterrupt(uint8_t Interrupt)
{
   uint8_t tmp;

   SPI3_Read(L3GD20H_CTRL3_ADDRESS, &tmp, 1);
   tmp &= ~Interrupt;
   SPI3_Write(L3GD20H_CTRL3_ADDRESS, &tmp, 1);
}
