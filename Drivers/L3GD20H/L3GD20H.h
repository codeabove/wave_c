/*
 * L3GD20H.h
 *
 *  Created on: 22.04.2017
 *      Author: Lukasz
 */

#ifndef __L3GD20H_H
#define __L3GD20H_H

/* L3GD20H registers addresses */
#define L3GD20H_WHO_AM_I_ADDRESS             ( 0x0F )
#define L3GD20H_CTRL1_ADDRESS                ( 0x20 )
#define L3GD20H_CTRL2_ADDRESS                ( 0x21 )
#define L3GD20H_CTRL3_ADDRESS                ( 0x22 )
#define L3GD20H_CTRL4_ADDRESS                ( 0x23 )
#define L3GD20H_CTRL5_ADDRESS                ( 0x24 )
#define L3GD20H_REFERENCE_ADDRESS            ( 0x25 )
#define L3GD20H_OUT_TEMP_ADDRESS             ( 0x26 )
#define L3GD20H_STATUS_ADDRESS               ( 0x27 )
#define L3GD20H_OUT_X_L_ADDRESS              ( 0x28 )
#define L3GD20H_OUT_X_H_ADDRESS              ( 0x29 )
#define L3GD20H_OUT_Y_L_ADDRESS              ( 0x2A )
#define L3GD20H_OUT_Y_H_ADDRESS              ( 0x2B )
#define L3GD20H_OUT_Z_L_ADDRESS              ( 0x2C )
#define L3GD20H_OUT_Z_H_ADDRESS              ( 0x2D )
#define L3GD20H_FIFO_CTRL_ADDRESS            ( 0x2E )
#define L3GD20H_FIFO_SRC_ADDRESS             ( 0x2F )
#define L3GD20H_IG_CFG_ADDRESS               ( 0x30 )
#define L3GD20H_IG_SRC_ADDRESS               ( 0x31 )
#define L3GD20H_IG_THS_XH_ADDRESS            ( 0x32 )
#define L3GD20H_IG_THS_XL_ADDRESS            ( 0x33 )
#define L3GD20H_IG_THS_YH_ADDRESS            ( 0x34 )
#define L3GD20H_IG_THS_YL_ADDRESS            ( 0x35 )
#define L3GD20H_IG_THS_ZH_ADDRESS            ( 0x36 )
#define L3GD20H_IG_THS_ZL_ADDRESS            ( 0x37 )
#define L3GD20H_IG_DURATION_ADDRESS          ( 0x38 )
#define L3GD20H_LOW_ODR_ADDRESS              ( 0x39 )

#define L3GD20H_WHO_AM_I                     ( (uint8_t)0xD7 )

/*** CTRL1 register***/
#define L3GD20H_OUTPUT_DATA_RATE_12_5HZ      ( (uint8_t)0x01 )
#define L3GD20H_OUTPUT_DATA_RATE_25HZ        ( (uint8_t)0x41 )
#define L3GD20H_OUTPUT_DATA_RATE_50HZ        ( (uint8_t)0x81 )
#define L3GD20H_OUTPUT_DATA_RATE_100HZ       ( (uint8_t)0x00 )
#define L3GD20H_OUTPUT_DATA_RATE_200HZ       ( (uint8_t)0x40 )
#define L3GD20H_OUTPUT_DATA_RATE_400HZ       ( (uint8_t)0x80 )
#define L3GD20H_OUTPUT_DATA_RATE_800HZ       ( (uint8_t)0xC0 )

#define L3GD20H_BANDWIDTH_0                  ( (uint8_t)0x00 )
#define L3GD20H_BANDWIDTH_1                  ( (uint8_t)0x10 )
#define L3GD20H_BANDWIDTH_2                  ( (uint8_t)0x20 )
#define L3GD20H_BANDWIDTH_3                  ( (uint8_t)0x30 )

#define L3GD20H_POWERMODE_DOWN               ( (uint8_t)0x00 )
#define L3GD20H_POWERMODE_NORMAL             ( (uint8_t)0x08 )

#define L3GD20H_X_ENABLE                     ( (uint8_t)0x02 )
#define L3GD20H_Y_ENABLE                     ( (uint8_t)0x01 )
#define L3GD20H_Z_ENABLE                     ( (uint8_t)0x04 )
#define L3GD20H_AXES_ENABLE                  ( (uint8_t)0x07 )
#define L3GD20H_AXES_DISABLE                 ( (uint8_t)0x00 )

/*** CTRL2 register ***/
#define L3GD20H_EXTERNALTRIGGER_DISABLE      ( (uint8_t)0x00 )
#define L3GD20H_EXTERNALTRIGGER_ENABLE       ( (uint8_t)0x80 )

#define L3GD20H_LEVELSENSTRIGGER_DISABLE     ( (uint8_t)0x00 )
#define L3GD20H_LEVELSENSTRIGGER_ENABLE      ( (uint8_t)0x40 )

#define L3GD20H_HPM_NORMAL_MODE_RES          ( (uint8_t)0x00 )
#define L3GD20H_HPM_REF_SIGNAL               ( (uint8_t)0x10 )
#define L3GD20H_HPM_NORMAL_MODE              ( (uint8_t)0x20 )
#define L3GD20H_HPM_AUTORESET_INT            ( (uint8_t)0x30 )

#define L3GD20H_HPCF_0                       ( (uint8_t)0x00 )
#define L3GD20H_HPCF_1                       ( (uint8_t)0x01 )
#define L3GD20H_HPCF_2                       ( (uint8_t)0x02 )
#define L3GD20H_HPCF_3                       ( (uint8_t)0x03 )
#define L3GD20H_HPCF_4                       ( (uint8_t)0x04 )
#define L3GD20H_HPCF_5                       ( (uint8_t)0x05 )
#define L3GD20H_HPCF_6                       ( (uint8_t)0x06 )
#define L3GD20H_HPCF_7                       ( (uint8_t)0x07 )
#define L3GD20H_HPCF_8                       ( (uint8_t)0x08 )
#define L3GD20H_HPCF_9                       ( (uint8_t)0x09 )

/*** CTRL3 register ***/
#define L3GD20H_INT1INTERRUPT_DISABLE        ( (uint8_t)0x00 )
#define L3GD20H_INT1INTERRUPT_ENABLE         ( (uint8_t)0x80 )

#define L3GD20H_INT1BOOTSTATUS_DISABLE       ( (uint8_t)0x00 )
#define L3GD20H_INT1BOOTSTATUS_ENABLE        ( (uint8_t)0x40 )

#define L3GD20H_INTACTIVECONFIG_HIGH         ( (uint8_t)0x20 )
#define L3GD20H_INTACTIVECONFIG_LOW          ( (uint8_t)0x00 )

#define L3GD20H_PP_OD_PUSHPULL               ( (uint8_t)0x00 )
#define L3GD20H_PP_OD_OPENDRAIN              ( (uint8_t)0x10 )

#define L3GD20H_INT2DATAREADY_DISABLE        ( (uint8_t)0x00 )
#define L3GD20H_INT2DATAREADY_ENABLE         ( (uint8_t)0x08 )

#define L3GD20H_INT2FIFOTHRESHOLD_DISABLE    ( (uint8_t)0x00 )
#define L3GD20H_INT2FIFOTHRESHOLD_ENABLE     ( (uint8_t)0x04 )

#define L3GD20H_INT2FIFOOVERRUN_DISABLE      ( (uint8_t)0x00 )
#define L3GD20H_INT2FIFOOVERRUN_ENABLE       ( (uint8_t)0x02 )

#define L3GD20H_INT2FIFOEMPTY_DISABLE        ( (uint8_t)0x00 )
#define L3GD20H_INT2FIFOEMPTY_ENABLE         ( (uint8_t)0x01 )

/*** CTRL4 register ***/
#define L3GD20H_BLOCKDATAUPDATE_CONTINUOS    ( (uint8_t)0x00 )
#define L3GD20H_BLOCKDATAUPDATE_SINGLE       ( (uint8_t)0x80 )

#define L3GD20H_BIGLITTLEENDIAN_LSB          ( (uint8_t)0x00 )
#define L3GD20H_BIGLITTLEENDIAN_MSB          ( (uint8_t)0x40 )

#define L3GD20H_FULLSCALE_250                ( (uint8_t)0x00 )
#define L3GD20H_FULLSCALE_500                ( (uint8_t)0x10 )
#define L3GD20H_FULLSCALE_2000               ( (uint8_t)0x20 )
#define L3GD20H_FULLSCALE_SELECTION          ( (uint8_t)0x30 )

#define L3GD20H_IMP_DISABLE                  ( (uint8_t)0x00 )
#define L3GD20H_IMP_ENABLE                   ( (uint8_t)0x80 )

#define L3GD20H_SELFTEST_NORMALMODE          ( (uint8_t)0x00 )
#define L3GD20H_SELFTEST_TEST0               ( (uint8_t)0x02 )
#define L3GD20H_SELFTEST_TEST1               ( (uint8_t)0x06 )

#define L3GD20H_SPIMODE_4WIRE                ( (uint8_t)0x00 )
#define L3GD20H_SPIMODE_3WIRE                ( (uint8_t)0x01 )

/*** CTRL5 register ***/
#define L3GD20H_BOOT_NORMALMODE              ( (uint8_t)0x00 )
#define L3GD20H_BOOT_REBOOTMEMORY            ( (uint8_t)0x80 )

#define L3GD20H_FIFO_DISABLE                 ( (uint8_t)0x00 )
#define L3GD20H_FIFO_ENABLE                  ( (uint8_t)0x40 )

#define L3GD20H_STOPONFTH_NOTLIMITED         ( (uint8_t)0x00 )
#define L3GD20H_STOPONFTH_LIMITED            ( (uint8_t)0x20 )

#define L3GD20H_HIGHPASSFILTER_DISABLE       ( (uint8_t)0x00 )
#define L3GD20H_HIGHPASSFILTER_ENABLE        ( (uint8_t)0x10 )

#define L3GD20H_IGSEL_00                     ( (uint8_t)0x00 )
#define L3GD20H_IGSEL_01                     ( (uint8_t)0x04 )
#define L3GD20H_IGSEL_10                     ( (uint8_t)0x08 )
#define L3GD20H_IGSEL_11                     ( (uint8_t)0x0C )

#define L3GD20H_OUTSEL_00                    ( (uint8_t)0x00 )
#define L3GD20H_OUTSEL_01                    ( (uint8_t)0x01 )
#define L3GD20H_OUTSEL_10                    ( (uint8_t)0x02 )
#define L3GD20H_OUTSEL_11                    ( (uint8_t)0x03 )

/*** FIFO_CTRL ***/
#define L3GD20H_FIFOMODE_BYPASS              ( (uint8_t)0x00 )
#define L3GD20H_FIFOMODE_FIFO                ( (uint8_t)0x20 )
#define L3GD20H_FIFOMODE_STREAM              ( (uint8_t)0x40 )
#define L3GD20H_FIFOMODE_STREAMTOFIFO        ( (uint8_t)0x60 )
#define L3GD20H_FIFOMODE_BYPASSTOSTREAM      ( (uint8_t)0x80 )
#define L3GD20H_FIFOMODE_DYNAMICSTREAM       ( (uint8_t)0xC0 )
#define L3GD20H_FIFOMODE_BYPASSTOFIFO        ( (uint8_t)0xE0 )

/*** Public functions ***/
void     L3GD20H_Initialize(void);
void     L3GD20H_Reboot(void);
void     L3GD20H_SetPowerMode(uint8_t PowerMode);

uint8_t  L3GD20H_GetID(void);
uint8_t  L3GD20H_GetDataStatus(void);
void     L3GD20H_GetRawAngleValues(int16_t *OutputBuffer);

void     L3GD20H_EnableInterrupt(uint8_t Interrupt);
void     L3GD20H_DisableInterrupt(uint8_t Interrupt);

#endif /* __L3GD20H_H */
