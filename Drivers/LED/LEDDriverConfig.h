/*
 * LEDDriverConfig.h
 *
 *  Created on: 15 sie 2017
 *      Author: Lukasz
 */

#ifndef __LEDDriverConfig_H_
#define __LEDDriverConfig_H_

#include "MCU/ST/stm32l4xx_ll_gpio.h"

typedef enum LED
{
   LED_Yellow = 0u,
   LED_Green,
   LED_Red,
   LED_Orange,
   /*************/
   LED_NumOf
} LED_T;

typedef struct LEDDriver_Pin
{
   GPIO_TypeDef *Port;
   uint32_t PinMask;
} LEDDriver_Pin_T;

extern const LEDDriver_Pin_T LEDs[LED_NumOf];

#endif /* __LEDDriverConfig_H_ */
