/*
 * LEDDriver.h
 *
 *  Created on: 15 sie 2017
 *      Author: Lukasz
 */

#ifndef __LEDDriver_H_
#define __LEDDriver_H_

#include "LEDDriverConfig.h"

void LED_On(uint8_t LedIndex);
void LED_Off(uint8_t LedIndex);
void LED_Toggle(uint8_t LedIndex);

#endif /* __LEDDriver_H_ */
