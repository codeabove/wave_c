/*
 * LEDDriver.c
 *
 *  Created on: 15 sie 2017
 *      Author: Lukasz
 */

#include "stm32l4xx.h"
#include "LEDDriver.h"
#include "LEDDriverConfig.h"

#include "MCU/ST/stm32l4xx_ll_gpio.h"

void LED_On(uint8_t LedIndex)
{
   LL_GPIO_SetOutputPin(LEDs[LedIndex].Port, LEDs[LedIndex].PinMask);
}

void LED_Off(uint8_t LedIndex)
{
   LL_GPIO_ResetOutputPin(LEDs[LedIndex].Port, LEDs[LedIndex].PinMask);
}

void LED_Toggle(uint8_t LedIndex)
{
   LL_GPIO_TogglePin(LEDs[LedIndex].Port, LEDs[LedIndex].PinMask);
}



