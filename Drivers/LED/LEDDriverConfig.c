/*
 * LEDDriverConfig.c
 *
 *  Created on: 15 sie 2017
 *      Author: Lukasz
 */

#include "LEDDriverConfig.h"

const LEDDriver_Pin_T LEDs[LED_NumOf] =
{
   [ LED_Green  ] = { .Port = GPIOA, .PinMask = LL_GPIO_PIN_7 },
   [ LED_Yellow ] = { .Port = GPIOA, .PinMask = LL_GPIO_PIN_5 },
   [ LED_Orange ] = { .Port = GPIOB, .PinMask = LL_GPIO_PIN_9 },
   [ LED_Red    ] = { .Port = GPIOB, .PinMask = LL_GPIO_PIN_8 },
};
