/*
 * ProximityDriverConfig.h
 *
 *  Created on: 29.03.2017
 *      Author: Lukasz
 */

#ifndef __ProximityDriverConfig_H
#define __ProximityDriverConfig_H

#include "MCU/ST/stm32l4xx_ll_adc.h"
#include "MCU/ST/stm32l4xx_ll_gpio.h"

typedef enum ProximityDriver_Sensor
{
   ProxDrv_Sensor_1 = 0u,
   ProxDrv_Sensor_2,
   ProxDrv_Sensor_3,
   ProxDrv_Sensor_4,
   ProxDrv_Sensor_NumOf
} ProximityDriver_Sensor_T;

typedef struct ProximityDriver_EmitterPin
{
   GPIO_TypeDef* Port;
   uint32_t PinMask;

} ProximityDriver_EmitterPin_T;

typedef struct ProximityDriver_DetectorPin
{
   ADC_TypeDef* ADC;
   uint32_t Rank;
} ProximityDriver_DetectorPin_T;

extern const ProximityDriver_EmitterPin_T Emitters[ProxDrv_Sensor_NumOf];
extern const ProximityDriver_DetectorPin_T Detectors[ProxDrv_Sensor_NumOf];

#endif /* __ProximityDriverConfig_H */
