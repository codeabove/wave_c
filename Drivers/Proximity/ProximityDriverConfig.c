/*
 * ProximityDriverConfig.c
 *
 *  Created on: 29.03.2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_adc.h"
#include "MCU/ST/stm32l4xx_ll_gpio.h"

#include "ProximityDriverConfig.h"

const ProximityDriver_EmitterPin_T Emitters[ProxDrv_Sensor_NumOf] =
{
      [ProxDrv_Sensor_1] = { .Port = GPIOA, .PinMask = LL_GPIO_PIN_0 },
      [ProxDrv_Sensor_2] = { .Port = GPIOA, .PinMask = LL_GPIO_PIN_1 },
      [ProxDrv_Sensor_3] = { .Port = GPIOA, .PinMask = LL_GPIO_PIN_2 },
      [ProxDrv_Sensor_4] = { .Port = GPIOA, .PinMask = LL_GPIO_PIN_3 },
};

const ProximityDriver_DetectorPin_T Detectors[ProxDrv_Sensor_NumOf] =
{
      [ProxDrv_Sensor_1] = { .ADC = ADC1, .Rank = LL_ADC_INJ_RANK_1 },
      [ProxDrv_Sensor_2] = { .ADC = ADC1, .Rank = LL_ADC_INJ_RANK_2 },
      [ProxDrv_Sensor_3] = { .ADC = ADC1, .Rank = LL_ADC_INJ_RANK_3 },
      [ProxDrv_Sensor_4] = { .ADC = ADC1, .Rank = LL_ADC_INJ_RANK_4 },
};
