/*
 * ProximityDriver.c
 *
 *  Created on: 29.03.2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_adc.h"
#include "MCU/ST/stm32l4xx_ll_gpio.h"
#include "MCU/ST/stm32l4xx_ll_tim.h"

#include "ProximityDriver.h"
#include "ProximityDriverConfig.h"

static void ProximityDriver_privReInitialize(void);
static void ProximityDriver_privEnableNextEmitter(void);
static void ProximityDriver_privDisableAllEmitters(void);
static void ProximityDriver_privTransferResults(void);
static void ProximityDriver_privClearResults(void);

typedef struct ProximityDriver
{
   const ProximityDriver_EmitterPin_T* Emitters;
   const ProximityDriver_DetectorPin_T* Detectors;
   volatile uint16_t Results[ProxDrv_Sensor_NumOf];
   volatile uint8_t ConversionsCounter;
} ProximityDriver_T;

static ProximityDriver_T ProximityDriver =
{
      .Emitters = Emitters,
      .Detectors = Detectors,
};

void ProximityDriver_Initialize(void)
{
   ProximityDriver.ConversionsCounter = 0;
   ProximityDriver_privClearResults();

   ProximityDriver_privDisableAllEmitters();
}

uint16_t ProximityDriver_GetResult(ProximityDriver_Sensor_T Sensor)
{
   if(Sensor < ProxDrv_Sensor_NumOf)
   {
      return ProximityDriver.Results[Sensor];
   }
   else
   {
      return 0u;
   }
}

void ProximityDriver_EndOfConversion_Callback(void)
{
   ProximityDriver_privDisableAllEmitters();
//   ProximityDriver_StopDelayTimer();

   ProximityDriver.ConversionsCounter++;

   if(ProximityDriver.ConversionsCounter < 4)
   {
      ProximityDriver_privEnableNextEmitter();
      ProximityDriver_StartDelayTimer();
   }
   else
   {
      ProximityDriver_privTransferResults();
      ProximityDriver_privReInitialize();
   }
}

void ProximityDriver_StartOfSequence_Callback(void)
{
   ProximityDriver_privEnableNextEmitter();
   ProximityDriver_StartDelayTimer();
}

void ProximityDriver_StartDelayTimer(void)
{
   LL_TIM_SetCounter(TIM2, 0);
   LL_TIM_EnableCounter(TIM2);
}

void ProximityDriver_StopDelayTimer(void)
{
   LL_TIM_DisableCounter(TIM2);
}

static void ProximityDriver_privReInitialize(void)
{
   ProximityDriver.ConversionsCounter = 0;
}

static void ProximityDriver_privEnableNextEmitter(void)
{
   LL_GPIO_SetOutputPin(ProximityDriver.Emitters[ProximityDriver.ConversionsCounter].Port,
                        ProximityDriver.Emitters[ProximityDriver.ConversionsCounter].PinMask);
}

static void ProximityDriver_privDisableAllEmitters(void)
{
   uint8_t i;

   for(i = 0; i < ProxDrv_Sensor_NumOf; i++)
   {
      LL_GPIO_ResetOutputPin(Emitters[i].Port, Emitters[i].PinMask);
   }
}

static void ProximityDriver_privTransferResults(void)
{
   uint8_t i;

   for(i = 0; i < ProxDrv_Sensor_NumOf; i++)
   {
      ProximityDriver.Results[i] = LL_ADC_INJ_ReadConversionData12(ProximityDriver.Detectors[i].ADC, ProximityDriver.Detectors[i].Rank);
   }
}

static void ProximityDriver_privClearResults(void)
{
   uint8_t i;

   for(i = 0; i < ProxDrv_Sensor_NumOf; i++)
   {
      ProximityDriver.Results[i] = 0;
   }
}
