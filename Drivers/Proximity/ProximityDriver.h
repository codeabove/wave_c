/*
 * ProximityDriver.h
 *
 *  Created on: 29.03.2017
 *      Author: Lukasz
 */

#ifndef __ProximityDriver_H
#define __ProximityDriver_H

#include "Drivers/Proximity/ProximityDriverConfig.h"

void ProximityDriver_Initialize(void);

uint16_t ProximityDriver_GetResult(ProximityDriver_Sensor_T Sensor);

//void ProximityDriver_StartPerform(void); //Enable?
//void ProximityDriver_StopPerform(void); //Disable?

void ProximityDriver_EndOfConversion_Callback(void);
void ProximityDriver_StartOfSequence_Callback(void);

void ProximityDriver_StartDelayTimer(void);
void ProximityDriver_StopDelayTimer(void);


#endif /* __ProximityDriver_H */
