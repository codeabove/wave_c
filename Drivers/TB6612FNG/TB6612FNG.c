/*
 * TB6612FNG.c
 *
 *  Created on: 27 sie 2017
 *      Author: Lukasz
 */

#include "stdlib.h"
#include "stm32l4xx.h"
#include "TB6612FNG.h"

#include "MCU/ST/stm32l4xx_ll_gpio.h"
#include "MCU/ST/stm32l4xx_ll_tim.h"

typedef void (*SetCompareFuncPtr)(TIM_TypeDef *TIMx, uint32_t CompareValue);

typedef struct TB6612FNG_Pin
{
   GPIO_TypeDef *Port;
   uint32_t PinMask;
} TB6612FNG_Pin_T;

typedef struct TB6612FNG_Pulse
{
   TIM_TypeDef *Timer;
   uint32_t ChannelMask;
   SetCompareFuncPtr SetCompare;
} TB6612FNG_Pulse_T;

typedef struct TB6612FNG_ChannelStruct
{
   TB6612FNG_Pin_T InputPin1;
   TB6612FNG_Pin_T InputPin2;
   TB6612FNG_Pulse_T PWM;

   uint16_t CompareValue;
   int16_t DutyCycle; // [0.1%]
   const TB6612FNG_Direction_T ForwardDirection;
   const TB6612FNG_Direction_T BackwardDirection;
   TB6612FNG_Direction_T Direction;
} TB6612FNG_Channel_S;

typedef struct TB6612FNG_Driver
{
   TB6612FNG_Channel_S Channels[TB6612FNG_Channel_NumOf];
   TB6612FNG_Pin_T Standby;
} TB6612FNG_Driver_T;

static TB6612FNG_Driver_T TB6612FNGDriver =
{
      .Channels =
      {
            [ TB6612FNG_Channel_A ] =
            {
                  .InputPin1 = { .Port = GPIOA, .PinMask = LL_GPIO_PIN_10 },
                  .InputPin2 = { .Port = GPIOB, .PinMask = LL_GPIO_PIN_14 },
                  .PWM =       { .Timer = TIM3, .ChannelMask = LL_TIM_CHANNEL_CH3, .SetCompare = &LL_TIM_OC_SetCompareCH3 },
            },
            [ TB6612FNG_Channel_B ] =
            {
                  .InputPin1 = { .Port = GPIOA, .PinMask = LL_GPIO_PIN_11 },
                  .InputPin2 = { .Port = GPIOA, .PinMask = LL_GPIO_PIN_12 },
                  .PWM =       { .Timer = TIM3, .ChannelMask = LL_TIM_CHANNEL_CH4, .SetCompare = &LL_TIM_OC_SetCompareCH4 },
            },
      },
      .Standby = { .Port = GPIOB, .PinMask = LL_GPIO_PIN_2 }
};

void TB6612FNG_Reset(void)
{
   TB6612FNG_EnableStandby();
   TB6612FNG_StopChannel(TB6612FNG_Channel_A);
   TB6612FNG_StopChannel(TB6612FNG_Channel_B);
}

void TB6612FNG_EnableStandby(void)
{
   LL_GPIO_ResetOutputPin(TB6612FNGDriver.Standby.Port, TB6612FNGDriver.Standby.PinMask);
}

void TB6612FNG_DisableStandby(void)
{
   LL_GPIO_SetOutputPin(TB6612FNGDriver.Standby.Port, TB6612FNGDriver.Standby.PinMask);
}

void TB6612FNG_SetChannel(TB6612FNG_Channel_T Channel, int16_t DutyCycle)
{
   //TODO: Add safety check to prevent sudden change of direction and burning chip
   uint16_t CompareValue;
   uint16_t AbsoluteDutyCycle;

   TB6612FNGDriver.Channels[Channel].DutyCycle = DutyCycle;

   AbsoluteDutyCycle = abs(DutyCycle);
   CompareValue = LL_TIM_GetAutoReload(TB6612FNGDriver.Channels[Channel].PWM.Timer) * AbsoluteDutyCycle / 1000u;

   if(DutyCycle >= 0)
   {
      TB6612FNGDriver.Channels[Channel].Direction = TB6612FNGDriver.Channels[Channel].ForwardDirection;
   }
   else
   {
      TB6612FNGDriver.Channels[Channel].Direction = TB6612FNGDriver.Channels[Channel].BackwardDirection;
   }

   if(TB6612FNG_Direction_CW == TB6612FNGDriver.Channels[Channel].Direction)
   {
      LL_GPIO_SetOutputPin(TB6612FNGDriver.Channels[Channel].InputPin1.Port, TB6612FNGDriver.Channels[Channel].InputPin1.PinMask);
      LL_GPIO_ResetOutputPin(TB6612FNGDriver.Channels[Channel].InputPin2.Port, TB6612FNGDriver.Channels[Channel].InputPin2.PinMask);
   }
   else if(TB6612FNG_Direction_CCW == TB6612FNGDriver.Channels[Channel].Direction)
   {
      LL_GPIO_ResetOutputPin(TB6612FNGDriver.Channels[Channel].InputPin1.Port, TB6612FNGDriver.Channels[Channel].InputPin1.PinMask);
      LL_GPIO_SetOutputPin(TB6612FNGDriver.Channels[Channel].InputPin2.Port, TB6612FNGDriver.Channels[Channel].InputPin2.PinMask);
   }
   else
   {
      //TODO: Nothing to do here?
   }

   TB6612FNGDriver.Channels[Channel].PWM.SetCompare(TB6612FNGDriver.Channels[Channel].PWM.Timer, CompareValue);
}

void TB6612FNG_StopChannel(TB6612FNG_Channel_T Channel)
{
   LL_GPIO_ResetOutputPin(TB6612FNGDriver.Channels[Channel].InputPin1.Port, TB6612FNGDriver.Channels[Channel].InputPin1.PinMask);
   LL_GPIO_ResetOutputPin(TB6612FNGDriver.Channels[Channel].InputPin2.Port, TB6612FNGDriver.Channels[Channel].InputPin2.PinMask);
   TB6612FNGDriver.Channels[Channel].PWM.SetCompare(TB6612FNGDriver.Channels[Channel].PWM.Timer, 0u);
}

int16_t TB6612FNG_GetDutyCycle(TB6612FNG_Channel_T Channel)
{
   return TB6612FNGDriver.Channels[Channel].DutyCycle;
}
