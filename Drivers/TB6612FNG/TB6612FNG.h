/*
 * TB6612FNG.h
 *
 *  Created on: 27 sie 2017
 *      Author: Lukasz
 */

#ifndef __TB6612FNG_H_
#define __TB6612FNG_H_

typedef enum TB6612FNG_Channel
{
   TB6612FNG_Channel_A = 0u,
   TB6612FNG_Channel_B,
   /*****************/
   TB6612FNG_Channel_NumOf
} TB6612FNG_Channel_T;

typedef enum TB6612FNG_Direction
{
   TB6612FNG_Direction_CW = 0u,
   TB6612FNG_Direction_CCW
} TB6612FNG_Direction_T;

void TB6612FNG_Reset(void);

void TB6612FNG_EnableStandby(void);
void TB6612FNG_DisableStandby(void);

void TB6612FNG_SetChannel(TB6612FNG_Channel_T Channel, int16_t DutyCycle);
void TB6612FNG_StopChannel(TB6612FNG_Channel_T Channel);

int16_t TB6612FNG_GetDutyCycle(TB6612FNG_Channel_T Channel);

#endif /* __TB6612FNG_H_ */
