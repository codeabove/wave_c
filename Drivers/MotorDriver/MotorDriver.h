/*
 * MotorDriver.h
 *
 *  Created on: 19 paź 2017
 *      Author: Lukasz
 */

#ifndef __MotorDriver_H_
#define __MotorDriver_H_

#include "MotorDriverConfig.h"

void MotorDriver_Perform(Motor_T Motor);
void MotorDriver_SetSpeed(Motor_T Motor, int16_t Speed);

#endif /* __MotorDriver_H_ */
