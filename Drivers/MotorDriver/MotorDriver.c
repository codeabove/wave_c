/*
 * MotorDriver.c
 *
 *  Created on: 19 paź 2017
 *      Author: Lukasz
 */

#include "stdint.h"
#include "stdlib.h"
#include "MotorDriver.h"
#include "MotorDriverConfig.h"
#include "Framework/PID/PID.h"
#include "Drivers/Encoder/EncoderDriver.h"
#include "Drivers/TB6612FNG/TB6612FNG.h"

typedef struct MotorParameters
{
   const uint16_t NominalSpeed; // [RPM]
   const uint16_t WheelDiameter; // [mm]
   const uint16_t WheelCircumference; // [mm]
} MotorParameters_T;

typedef struct MotorDriver
{
   const MotorParameters_T MotorParameters;
   PID_T SpeedPID;
   Encoder_T Encoder;
   uint8_t Channel;

   int16_t TargetSpeed; // [mm/s]
} MotorDriver_T;

static MotorDriver_T MotorDrivers[Motor_NumOf] =
{
      [Motor_Left] =
      {
            .MotorParameters =
            {
                  .NominalSpeed = 1000u,
                  .WheelDiameter = 37u,
                  .WheelCircumference = 116u
            },
            .SpeedPID =
            {
                  .GainP = 0.25f,
                  .GainI = 1.0f,
                  .GainD = 0.0f,
                  .IterationTime = 0.001f,
                  .OutputMin = -1000.0f,
                  .OutputMax = 1000.0f
            },
            .Encoder = 1u, //Encoder_Left
            .Channel = 0u, //TB6612FNG_Channel_A
      },
      [Motor_Right] =
      {
            .MotorParameters =
            {
                  .NominalSpeed = 1000u,
                  .WheelDiameter = 37u,
                  .WheelCircumference = 116u
            },
            .SpeedPID =
            {
                  .GainP = 0.25f,
                  .GainI = 1.0f,
                  .GainD = 0.0f,
                  .IterationTime = 0.001f,
                  .OutputMin = -1000.0f,
                  .OutputMax = 1000.0f
            },
            .Encoder = 0u, //Encoder_Right
            .Channel = 1u, //TB6612FNG_Channel_B
      }
};

void MotorDriver_Perform(Motor_T Motor)
{
   int16_t NewTargetSpeed_RPM;
   int16_t MotorSpeed_RPM;
   int16_t MotorRegulatedSpeed_RPM;
   int16_t DutyCycle;

   /* First convert target speed given in mm/s to RPM */
   NewTargetSpeed_RPM = MotorDrivers[Motor].TargetSpeed * 60 / MotorDrivers[Motor].MotorParameters.WheelCircumference;

   /* Next get measured speed and convert it to RPM */
   MotorSpeed_RPM = MotorDriverConfig_GetEncoderSpeed(MotorDrivers[Motor].Encoder);

   /* Now apply both to PID regulator and get output */
   MotorRegulatedSpeed_RPM = (int16_t)PID_Process(&MotorDrivers[Motor].SpeedPID, NewTargetSpeed_RPM, MotorSpeed_RPM);

   /* Then calculate PWM duty cycle. Multiply by 1000 to get [0.1%] units. */
   DutyCycle = MotorRegulatedSpeed_RPM * 1000 / MotorDrivers[Motor].MotorParameters.NominalSpeed;

   /* Finally apply value to motor */
   MotorDriverConfig_SetMotorDutyCycle(MotorDrivers[Motor].Channel, DutyCycle);
}

void MotorDriver_SetSpeed(Motor_T Motor, int16_t Speed)
{
   MotorDrivers[Motor].TargetSpeed = Speed;
}
