/*
 * MotorDriverConfig.h
 *
 *  Created on: 22 paź 2017
 *      Author: Lukasz
 */

#ifndef __MotorDriverConfig_H_
#define __MotorDriverConfig_H_

#include "stdint.h"

typedef enum MotorDriverEnum
{
   Motor_Left = 0u,
   Motor_Right,
   /**************/
   Motor_NumOf
} Motor_T;

uint32_t MotorDriverConfig_GetEncoderSpeed(uint8_t Encoder);
void MotorDriverConfig_SetMotorDutyCycle(uint8_t Channel, int16_t DutyCycle);

#endif /* __MotorDriverConfig_H_ */
