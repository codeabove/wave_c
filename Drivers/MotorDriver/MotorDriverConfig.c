/*
 * MotorDriverConfig.c
 *
 *  Created on: 22 paź 2017
 *      Author: Lukasz
 */

#include "stdlib.h"
#include "MotorDriverConfig.h"
#include "Drivers/Encoder/EncoderDriver.h"
#include "Drivers/TB6612FNG/TB6612FNG.h"

uint32_t MotorDriverConfig_GetEncoderSpeed(uint8_t Encoder)
{
   return EncoderDriver_GetCurrentSpeed_RPM((Encoder_T)Encoder);
}

void MotorDriverConfig_SetMotorDutyCycle(uint8_t Channel, int16_t DutyCycle)
{
   TB6612FNG_SetChannel(Channel, DutyCycle);
}
