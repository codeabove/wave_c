/*
 * USART1.h
 *
 *  Created on: 1 paź 2017
 *      Author: Lukasz
 */

#ifndef __USART1_H_
#define __USART1_H_

#include "stdbool.h"

void USART1_Initialize(void);

void USART1_Write(uint8_t Data);
void USART1_EnableTransmitInterrupt(void);
void USART1_DisableTransmitInterrupt(void);
void USART1_RequestTxInterrupt(void);
bool USART1_IsTxTransmissionInProgress(void);

#endif /* __USART1_H_ */
