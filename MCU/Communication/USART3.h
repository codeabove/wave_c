/*
 * USART3.h
 *
 *  Created on: 04.04.2017
 *      Author: Lukasz
 */

#ifndef __USART3_H
#define __USART3_H

#include "stdbool.h"

void USART3_Initialize(void);

void USART3_Write(uint8_t Data);
void USART3_EnableTransmitInterrupt(void);
void USART3_DisableTransmitInterrupt(void);
void USART3_RequestTxInterrupt(void);
bool USART3_IsTxTransmissionInProgress(void);

#endif /* __USART3_H */
