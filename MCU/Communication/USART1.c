/*
 * USART1.c
 *
 *  Created on: 1 paź 2017
 *      Author: Lukasz
 */

#include "stdbool.h"
#include "MCU/ST/stm32l4xx_ll_usart.h"

#include "USART1.h"

#define BAUDRATE ( (uint32_t)( 9600 ) )

void USART1_Initialize(void)
{
   LL_USART_ConfigCharacter(USART1, LL_USART_DATAWIDTH_8B, LL_USART_PARITY_NONE, LL_USART_STOPBITS_1);
   LL_USART_SetBaudRate(USART1, SystemCoreClock, LL_USART_OVERSAMPLING_16, BAUDRATE);
   LL_USART_EnableDirectionTx(USART1);
   LL_USART_EnableDirectionRx(USART1);
   LL_USART_Enable(USART1);

   NVIC_EnableIRQ(USART1_IRQn);
   LL_USART_EnableIT_RXNE(USART1);
}

void USART1_Write(uint8_t Data)
{
   LL_USART_TransmitData8(USART1, Data);
}

void USART1_EnableTransmitInterrupt(void)
{
   LL_USART_EnableIT_TXE(USART1);
}

void USART1_DisableTransmitInterrupt(void)
{
   LL_USART_DisableIT_TXE(USART1);
}

void USART1_RequestTxInterrupt(void)
{
   LL_USART_RequestTxDataFlush(USART1);
}

bool USART1_IsTxTransmissionInProgress(void)
{
   return LL_USART_IsActiveFlag_TC(USART1);
}

extern void ModbusRTU_ReceiveCallback(uint8_t Byte);
extern void ModbusRTU_TransmitCallback(void);
void USART1_IRQHandler(void)
{
   // Handling incoming data has higher priority.
   // Don't want to be late and miss something.
   if(LL_USART_IsActiveFlag_RXNE(USART1))
   {
      ModbusRTU_ReceiveCallback(LL_USART_ReceiveData8(USART1));
   }
   else if(LL_USART_IsActiveFlag_ORE(USART1))
   {
      LL_USART_ClearFlag_ORE(USART1);
   }
   else if(LL_USART_IsActiveFlag_TXE(USART1))
   {
//      HC08_TransmitCallback();
      ModbusRTU_TransmitCallback();
   }
}

