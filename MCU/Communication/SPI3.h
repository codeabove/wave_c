/*
 * SPI3.h
 *
 *  Created on: 22.04.2017
 *      Author: Lukasz
 */

#ifndef __SPI3_H
#define __SPI3_H

#define SPI_TxBufferSize ( 32u )

void SPI3_Initialize(void);
void SPI3_Enable(void);

void SPI3_Read(uint8_t Address, uint8_t* Buffer, uint16_t NumberOfBytes);
void SPI3_Write(uint8_t Address, uint8_t* Buffer, uint16_t NumberOfBytes);

#endif /* __SPI3_H */
