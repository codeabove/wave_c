/*
 * SPI3.c
 *
 *  Created on: 22.04.2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_spi.h"
#include "Framework/FIFO/FIFO.h"
#include "SPI3.h"

#define MultipleByteMask   ( (uint8_t) 0x40 )
#define ReadOnlyMask       ( (uint8_t) 0x80 )

#define DummyByte          ( (uint8_t) 0x00 )

volatile uint8_t SPI3_Response;
volatile bool DataReady = false;

void SPI3_Initialize(void)
{
   LL_SPI_SetBaudRatePrescaler(SPI3, LL_SPI_BAUDRATEPRESCALER_DIV256);
   LL_SPI_SetClockPolarity(SPI3, LL_SPI_POLARITY_HIGH);
   LL_SPI_SetClockPhase(SPI3, LL_SPI_PHASE_2EDGE);
   LL_SPI_SetTransferDirection(SPI3, LL_SPI_FULL_DUPLEX);
   LL_SPI_SetTransferBitOrder(SPI3, LL_SPI_MSB_FIRST);
   LL_SPI_SetMode(SPI3, LL_SPI_MODE_MASTER);

   LL_SPI_SetDataWidth(SPI3, LL_SPI_DATAWIDTH_8BIT);
   LL_SPI_SetNSSMode(SPI3, LL_SPI_NSS_HARD_OUTPUT);
   LL_SPI_SetStandard(SPI3, LL_SPI_PROTOCOL_MOTOROLA);
   LL_SPI_DisableNSSPulseMgt(SPI3);
   LL_SPI_SetRxFIFOThreshold(SPI3, LL_SPI_RX_FIFO_TH_QUARTER);

   LL_SPI_EnableIT_RXNE(SPI3);
//   LL_SPI_EnableIT_TXE(SPI3);

   NVIC_EnableIRQ(SPI3_IRQn);
}

void SPI3_Enable(void)
{
   LL_SPI_Enable(SPI3);
}

void SPI3_Disable(void)
{
   while(LL_SPI_GetTxFIFOLevel(SPI3) != 0) {}
   while(LL_SPI_IsActiveFlag_BSY(SPI3)) {}
   LL_SPI_Disable(SPI3);
}

void SPI3_Read(uint8_t Address, uint8_t* Buffer, uint16_t NumberOfBytes)
{
   if(NumberOfBytes > 1)
   {
      Address |= (uint8_t)(ReadOnlyMask | MultipleByteMask);
   }
   else
   {
      Address |= (uint8_t)(ReadOnlyMask);
   }

   SPI3_Enable();

   LL_SPI_TransmitData8(SPI3, Address);

   while(NumberOfBytes > 0u)
   {
      LL_SPI_TransmitData8(SPI3, DummyByte);
      while(DataReady == false);
      *Buffer = SPI3_Response;
      NumberOfBytes--;
      Buffer++;

      DataReady = false;
   }

   SPI3_Disable();
}

void SPI3_Write(uint8_t Address, uint8_t* Buffer, uint16_t NumberOfBytes)
{
   if(NumberOfBytes > 1)
   {
      Address |= (uint8_t)(MultipleByteMask);
   }

   SPI3_Enable();

   LL_SPI_TransmitData8(SPI3, Address);

   while(NumberOfBytes > 0u)
   {
      LL_SPI_TransmitData8(SPI3, *Buffer);
      NumberOfBytes--;
      Buffer++;
   }

   SPI3_Disable();
}

void SPI3_IRQHandler(void)
{
   if(LL_SPI_IsActiveFlag_RXNE(SPI3))
   {
      DataReady = true;
      SPI3_Response = LL_SPI_ReceiveData8(SPI3);
//      Logger_SendBinary(SPI3_Response);
   }
//   else if(LL_SPI_IsActiveFlag_TXE(SPI3))
//   {
//      if(NumberOfBytes > 0u)
//      {
//         LL_SPI_TransmitData8(SPI3, *Buffer);
//         NumberOfBytes--;
//         Buffer++;
//      }
//
//      SPI3_Disable();
//   }
//   SPI3_Disable();
}
