/*
 * USART3.c
 *
 *  Created on: 04.04.2017
 *      Author: Lukasz
 */

#include "stdbool.h"

#include "MCU/ST/stm32l4xx_ll_usart.h"

#include "USART3.h"

#define BAUDRATE ( (uint32_t)( 9600 ) )

void USART3_Initialize(void)
{
   LL_USART_ConfigCharacter(USART3, LL_USART_DATAWIDTH_8B, LL_USART_PARITY_NONE, LL_USART_STOPBITS_1);
   LL_USART_SetBaudRate(USART3, SystemCoreClock, LL_USART_OVERSAMPLING_16, BAUDRATE);
   LL_USART_EnableDirectionTx(USART3);
   LL_USART_Enable(USART3);

   NVIC_EnableIRQ(USART3_IRQn);
}

void USART3_Write(uint8_t Data)
{
   LL_USART_TransmitData8(USART3, Data);
}

void USART3_EnableTransmitInterrupt(void)
{
   LL_USART_EnableIT_TXE(USART3);
}

void USART3_DisableTransmitInterrupt(void)
{
   LL_USART_DisableIT_TXE(USART3);
}

void USART3_RequestTxInterrupt(void)
{
   LL_USART_RequestTxDataFlush(USART3);
}

bool USART3_IsTxTransmissionInProgress(void)
{
   return LL_USART_IsActiveFlag_TC(USART3);
}

extern void Logger_TransmitCallback(void);
void USART3_IRQHandler(void)
{
   if(LL_USART_IsActiveFlag_TXE(USART3))
   {
      Logger_TransmitCallback();
   }
}
