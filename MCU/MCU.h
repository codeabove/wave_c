/*
 * MCU.h
 *
 *  Created on: 06.03.2017
 *      Author: Lukasz
 */

#ifndef __MCU_H
#define __MCU_H

void MCU_Initialize(void);
void MCU_EnableDebugFeatures(void);

#endif /* __MCU_H */
