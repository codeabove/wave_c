/*
 * SystemClock.h
 *
 *  Created on: 20.04.2017
 *      Author: Lukasz
 */

#ifndef __SystemClock_H
#define __SystemClock_H

void SystemClock_SetMaxFrequency(void);

#endif /* __SystemClock_H */
