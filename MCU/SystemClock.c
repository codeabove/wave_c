/*
 * SystemClock.c
 *
 *  Created on: 20.04.2017
 *      Author: Lukasz
 */

#include "ST/stm32l4xx_ll_rcc.h"
#include "ST/stm32l4xx_ll_system.h"

#include "SystemClock.h"

#define RCC_PLLN_FACTOR ( 20u )
//#define USE_HSE

void SystemClock_SetMaxFrequency(void)
{
   LL_FLASH_SetLatency(LL_FLASH_LATENCY_4);
   while(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_4);

#ifdef USE_HSE
   LL_RCC_HSE_Enable();
   while(!(LL_RCC_HSE_IsReady()));

   LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLM_DIV_1, RCC_PLLN_FACTOR, LL_RCC_PLLR_DIV_2);
#else
   LL_RCC_HSI_Enable();
   while(!(LL_RCC_HSI_IsReady()));

   LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI, LL_RCC_PLLM_DIV_1, RCC_PLLN_FACTOR, LL_RCC_PLLR_DIV_4);
#endif
   LL_RCC_PLL_EnableDomain_SYS();

   LL_RCC_PLL_Enable();
   while(!(LL_RCC_PLL_IsReady()));

   LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
   LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
   LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);

   LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
   while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL);

   SystemCoreClockUpdate();
}

