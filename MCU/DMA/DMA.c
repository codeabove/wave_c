/*
 * DMA.c
 *
 *  Created on: 20.04.2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_dma.h"

#include "DMA.h"

volatile uint16_t res[4];

void DMA_Initialize(void)
{
//   LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_1, (uint32_t)&ADC1->JDR1);
//   LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PERIPH_INCREMENT);
//   LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_1, (uint32_t)&res);
//   LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MEMORY_INCREMENT);
//
//   LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_1, 4);
//
//   LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_1, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
//   LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MODE_CIRCULAR);
//
//   LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PDATAALIGN_HALFWORD);
//   LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MDATAALIGN_HALFWORD);
//
//   LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PRIORITY_VERYHIGH);
//
//   NVIC_EnableIRQ(DMA1_Channel1_IRQn);
//   LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_1);
//   LL_DMA_EnableIT_HT(DMA1, LL_DMA_CHANNEL_1);
//   LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_1);
//
//   LL_DMA_SetPeriphRequest(DMA1, LL_DMA_CHANNEL_1, LL_DMA_REQUEST_0);
//   LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_1);
}

void DMA1_Channel1_IRQHandler(void)
{
//   if(LL_DMA_IsActiveFlag_TC1(DMA1))
//   {
//      LL_DMA_ClearFlag_TC1(DMA1);
//   }
}
