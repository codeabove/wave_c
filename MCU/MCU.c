/*
 * MCU.c
 *
 *  Created on: 06.03.2017
 *      Author: Lukasz
 */

#include "ST/stm32l4xx_ll_adc.h"
#include "ST/stm32l4xx_ll_bus.h"
#include "ST/stm32l4xx_ll_dma.h"
#include "ST/stm32l4xx_ll_gpio.h"
#include "ST/stm32l4xx_ll_rcc.h"
#include "ST/stm32l4xx_ll_system.h"
#include "ST/stm32l4xx_ll_tim.h"

#include "ADC/ADC.h"
#include "Communication/SPI3.h"
#include "Communication/USART1.h"
#include "Communication/USART3.h"
#include "DMA/DMA.h"

#include "Timers/SysTick.h"
#include "Timers/TIM1.h"
#include "Timers/TIM2.h"
#include "Timers/TIM3.h"
#include "Timers/TIM4.h"
#include "Timers/TIM6.h"
#include "Timers/TIM7.h"
#include "Timers/TIM8.h"
#include "Timers/TIM15.h"
#include "Timers/TIM16.h"

#include "GPIO.h"
#include "RCC.h"
#include "SystemClock.h"

#include "MCU.h"

void MCU_Initialize(void)
{
   SystemClock_SetMaxFrequency();

   SysTick_Initialize();

   RCC_Initialize();
   GPIO_Initialize();

   /* Timers:
    * TIM1  - Encoder
    * TIM2  - Proximity
    * TIM3  - Motor PWM
    * TIM4  - Proximity
    * TIM5  -
    * TIM6  - Modbus RTU T1.5
    * TIM7  - Modbus RTU T3.5
    * TIM8  - Encoder
    * TIM15 - Battery ADC
    * TIM16 - Buzzer
    * TIM17 - Application performance and timing
    */
   TIM1_Initialize();
   TIM2_Initialize();
   TIM3_Initialize();
   TIM4_Initialize();
   TIM6_Initialize();
   TIM7_Initialize();
   TIM8_Initialize();
   TIM15_Initialize();
   TIM16_Initialize();

   /*
    * ADCs:
    * ADC1 - Proximity sensors (injected)
    * ADC2 - Battery measurement (regular)
    */
   ADC_Initialize();

   /*
    * USARTs:
    * USART1 - Bluetooth
    * USART2 - Not Used
    * USART3 - Logger
    */
   USART1_Initialize();
   USART3_Initialize();

   /* SPIs:
    * SPI1 - Not Used
    * SPI2 - Not Used
    * SPI3 - Gyroscope
    */
   SPI3_Initialize();

}

void MCU_EnableDebugFeatures(void)
{
//   LL_DBGMCU_APB1_GRP1_FreezePeriph(LL_DBGMCU_APB1_GRP1_TIM2_STOP);
//   LL_DBGMCU_APB1_GRP1_FreezePeriph(LL_DBGMCU_APB1_GRP1_TIM3_STOP);
//   LL_DBGMCU_APB1_GRP1_FreezePeriph(LL_DBGMCU_APB1_GRP1_TIM4_STOP);
//
//   LL_DBGMCU_APB2_GRP1_FreezePeriph(LL_DBGMCU_APB2_GRP1_TIM16_STOP);
}
