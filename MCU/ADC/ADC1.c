/*
 * ADC1.c
 *
 *  Created on: 20.04.2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_adc.h"

#include "ADC1.h"

void ADC1_Initialize(void)
{
   /* By default ADC is in deep-power-down mode, disable it */
   LL_ADC_DisableDeepPowerDown(ADC1);

   /* Enable internal voltage regulator */
   LL_ADC_EnableInternalRegulator(ADC1);

   //TODO: Add wait required time
   //delay_us(LL_ADC_DELAY_INTERNAL_REGUL_STAB_US);

   LL_ADC_StartCalibration(ADC1, LL_ADC_SINGLE_ENDED);
   while(LL_ADC_IsCalibrationOnGoing(ADC1));

   LL_ADC_Enable(ADC1);

   LL_ADC_INJ_SetSequencerLength(ADC1, LL_ADC_INJ_SEQ_SCAN_ENABLE_4RANKS);
   LL_ADC_INJ_SetSequencerDiscont(ADC1, LL_ADC_INJ_SEQ_DISCONT_1RANK);

   LL_ADC_INJ_SetSequencerRanks(ADC1, LL_ADC_INJ_RANK_1, LL_ADC_CHANNEL_1);
   LL_ADC_INJ_SetSequencerRanks(ADC1, LL_ADC_INJ_RANK_2, LL_ADC_CHANNEL_2);
   LL_ADC_INJ_SetSequencerRanks(ADC1, LL_ADC_INJ_RANK_3, LL_ADC_CHANNEL_3);
   LL_ADC_INJ_SetSequencerRanks(ADC1, LL_ADC_INJ_RANK_4, LL_ADC_CHANNEL_4);

   LL_ADC_SetChannelSamplingTime(ADC1, LL_ADC_CHANNEL_1, LL_ADC_SAMPLINGTIME_2CYCLES_5);
   LL_ADC_SetChannelSamplingTime(ADC1, LL_ADC_CHANNEL_2, LL_ADC_SAMPLINGTIME_2CYCLES_5);
   LL_ADC_SetChannelSamplingTime(ADC1, LL_ADC_CHANNEL_3, LL_ADC_SAMPLINGTIME_2CYCLES_5);
   LL_ADC_SetChannelSamplingTime(ADC1, LL_ADC_CHANNEL_4, LL_ADC_SAMPLINGTIME_2CYCLES_5);

   LL_ADC_SetResolution(ADC1, LL_ADC_RESOLUTION_12B);
   LL_ADC_SetDataAlignment(ADC1, LL_ADC_DATA_ALIGN_RIGHT);

   LL_ADC_INJ_SetTriggerSource(ADC1, LL_ADC_INJ_TRIG_EXT_TIM2_CH1);
   LL_ADC_INJ_SetTriggerEdge(ADC1, LL_ADC_INJ_TRIG_EXT_RISINGFALLING);

   LL_ADC_EnableIT_JEOC(ADC1);

   //TODO: Is it needed?
   LL_ADC_EnableIT_JEOS(ADC1);

   //TODO: Where to do this? Maybe after all inits enable peripherals?
   LL_ADC_INJ_StartConversion(ADC1);
}
