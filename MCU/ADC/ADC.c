/*
 * ADC.c
 *
 *  Created on: 20.04.2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_adc.h"

#include "ADC.h"
#include "ADC1.h"
#include "ADC2.h"

volatile uint16_t ADC_Result;

void ADC_Initialize(void)
{
   /* Clock derived from AHB to synchronize with timers */
   LL_ADC_SetCommonClock(ADC123_COMMON, LL_ADC_CLOCK_SYNC_PCLK_DIV1);

   ADC1_Initialize();
   ADC2_Initialize();

   NVIC_EnableIRQ(ADC1_2_IRQn);
}

extern void ProximityDriver_EndOfConversion_Callback(void);
extern void BatteryMeter_ApplyNewValue(uint16_t NewValue);
void ADC1_2_IRQHandler(void)
{
   if(LL_ADC_IsActiveFlag_JEOC(ADC1))
   {
      ProximityDriver_EndOfConversion_Callback();

      LL_ADC_ClearFlag_JEOC(ADC1);
   }
   else if(LL_ADC_IsActiveFlag_JEOS(ADC1))
   {
      LL_ADC_ClearFlag_JEOS(ADC1);
   }
   else if(LL_ADC_IsActiveFlag_EOC(ADC2))
   {
      LL_ADC_ClearFlag_EOC(ADC2);
      ADC_Result = LL_ADC_REG_ReadConversionData12(ADC2);
      BatteryMeter_ApplyNewValue(ADC_Result);

      //TODO: Read and apply result where needed (Battery Meter)
   }
   else
   {

   }
}

