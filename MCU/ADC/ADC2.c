/*
 * ADC2.c
 *
 *  Created on: 16 sie 2017
 *      Author: Lukasz
 */

#include "ADC1.h"
#include "MCU/ST/stm32l4xx_ll_adc.h"

void ADC2_Initialize(void)
{
   /* By default ADC is in deep-power-down mode, disable it */
   LL_ADC_DisableDeepPowerDown(ADC2);

   /* Enable internal voltage regulator */
   LL_ADC_EnableInternalRegulator(ADC2);

   //TODO: Add wait required time
   //delay_us(LL_ADC_DELAY_INTERNAL_REGUL_STAB_US);

   LL_ADC_StartCalibration(ADC2, LL_ADC_SINGLE_ENDED);
   while(LL_ADC_IsCalibrationOnGoing(ADC2));

   LL_ADC_Enable(ADC2);

   LL_ADC_REG_SetSequencerLength(ADC2, LL_ADC_REG_SEQ_SCAN_DISABLE);
   LL_ADC_REG_SetSequencerDiscont(ADC2, LL_ADC_REG_SEQ_DISCONT_DISABLE);

   LL_ADC_REG_SetContinuousMode(ADC2, LL_ADC_REG_CONV_SINGLE);

   LL_ADC_REG_SetSequencerRanks(ADC2, LL_ADC_REG_RANK_1, LL_ADC_CHANNEL_9);

   LL_ADC_SetResolution(ADC2, LL_ADC_RESOLUTION_12B);
   LL_ADC_SetDataAlignment(ADC2, LL_ADC_DATA_ALIGN_RIGHT);
   LL_ADC_SetChannelSamplingTime(ADC2, LL_ADC_CHANNEL_9, LL_ADC_SAMPLINGTIME_640CYCLES_5);

   LL_ADC_REG_SetOverrun(ADC2, LL_ADC_REG_OVR_DATA_OVERWRITTEN);

   LL_ADC_REG_SetTriggerSource(ADC2, LL_ADC_REG_TRIG_EXT_TIM15_TRGO);

   LL_ADC_EnableIT_EOC(ADC2);
}
