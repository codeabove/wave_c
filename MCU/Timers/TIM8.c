/*
 * TIM8.c
 *
 *  Created on: 17 wrz 2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_tim.h"

#include "TIM8.h"

void TIM8_Initialize(void)
{
   LL_TIM_SetEncoderMode(TIM8, LL_TIM_ENCODERMODE_X4_TI12);
}

void TIM8_EnableCounter(void)
{
   LL_TIM_EnableCounter(TIM8);
}

void TIM8_DisableCounter(void)
{
   LL_TIM_DisableCounter(TIM8);
}


