/*
 * SysTick.h
 *
 *  Created on: 04.04.2017
 *      Author: Lukasz
 */

#ifndef __SysTick_H
#define __SysTick_H

#include "stdbool.h"

void SysTick_Initialize(void);
void SysTick_Enable(void);

bool SysTick_Is1msPassed(void);
bool SysTick_Is10msPassed(void);
bool SysTick_Is100msPassed(void);

#endif /* __SysTick_H */
