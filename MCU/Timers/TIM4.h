/*
 * TIM4.h
 *
 *  Created on: 30.03.2017
 *      Author: Lukasz
 */

#ifndef __TIM4_H
#define __TIM4_H

#define TIM4_SOURCECLOCK_FREQ ( 80000000u )
#define TIM4_TIMERCLOCK_FREQ  ( 1000000u )
#define TIM4_FREQ             ( 1000u )

void TIM4_Initialize(void);

void TIM4_EnableCounter(void);
void TIM4_DisableCounter(void);

#endif /* __TIM4_H */
