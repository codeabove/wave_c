/*
 * TIM4.c
 *
 *  Created on: 30.03.2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_tim.h"

#include "TIM4.h"

void TIM4_Initialize(void)
{
   LL_TIM_SetClockSource(TIM4, LL_TIM_CLOCKSOURCE_INTERNAL);
   LL_TIM_SetPrescaler(TIM4, __LL_TIM_CALC_PSC(TIM4_SOURCECLOCK_FREQ, TIM4_TIMERCLOCK_FREQ));
   LL_TIM_SetAutoReload(TIM4, __LL_TIM_CALC_ARR(TIM4_SOURCECLOCK_FREQ, LL_TIM_GetPrescaler(TIM4), TIM4_FREQ));

   NVIC_EnableIRQ(TIM4_IRQn);
   LL_TIM_EnableIT_UPDATE(TIM4);
}

void TIM4_EnableCounter(void)
{
   LL_TIM_EnableCounter(TIM4);
}

void TIM4_DisableCounter(void)
{
   LL_TIM_DisableCounter(TIM4);
}

extern void ProximityDriver_StartOfSequence_Callback(void);
void TIM4_IRQHandler(void)
{
   if(LL_TIM_IsActiveFlag_UPDATE(TIM4))
   {
      ProximityDriver_StartOfSequence_Callback();
      LL_TIM_ClearFlag_UPDATE(TIM4);
   }
}
