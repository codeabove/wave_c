/*
 * TIM8.h
 *
 *  Created on: 17 wrz 2017
 *      Author: Lukasz
 */

#ifndef __TIM8_H_
#define __TIM8_H_

void TIM8_Initialize(void);

void TIM8_EnableCounter(void);
void TIM8_DisableCounter(void);


#endif /* __TIM8_H_ */
