/*
 * TIM1.c
 *
 *  Created on: 16 wrz 2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_tim.h"

#include "TIM1.h"

void TIM1_Initialize(void)
{
   LL_TIM_SetEncoderMode(TIM1, LL_TIM_ENCODERMODE_X4_TI12);
}

void TIM1_EnableCounter(void)
{
   LL_TIM_EnableCounter(TIM1);
}

void TIM1_DisableCounter(void)
{
   LL_TIM_DisableCounter(TIM1);
}

