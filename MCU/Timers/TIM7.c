/*
 * TIM7.c
 *
 *  Created on: 7 lut 2018
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_tim.h"

#include "TIM7.h"
#include "Drivers/LED/LEDDriver.h"

void TIM7_Initialize(void)
{
   LL_TIM_SetOnePulseMode(TIM7, LL_TIM_ONEPULSEMODE_SINGLE);
   LL_TIM_SetPrescaler(TIM7, __LL_TIM_CALC_PSC(TIM7_SOURCECLOCK_FREQ, TIM7_TIMERCLOCK_FREQ));
   LL_TIM_SetAutoReload(TIM7, __LL_TIM_CALC_DELAY(TIM7_SOURCECLOCK_FREQ, LL_TIM_GetPrescaler(TIM7), TIM7_DELAY_9600BR));

   LL_TIM_SetUpdateSource(TIM7, LL_TIM_UPDATESOURCE_COUNTER);

   NVIC_EnableIRQ(TIM7_IRQn);
   LL_TIM_EnableIT_UPDATE(TIM7);
}

void TIM7_Start(void)
{
   LL_TIM_EnableCounter(TIM7);
}

void TIM7_ResetCounter(void)
{
   LL_TIM_GenerateEvent_UPDATE(TIM7);
}

extern void ModbusRTU_TimerT35Callback(void);
void TIM7_IRQHandler(void)
{
   if (LL_TIM_IsActiveFlag_UPDATE(TIM7))
   {
      ModbusRTU_TimerT35Callback();
      LL_TIM_ClearFlag_UPDATE(TIM7);
   }
}
