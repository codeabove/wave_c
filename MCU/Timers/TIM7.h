/*
 * TIM7.h
 *
 *  Created on: 7 lut 2018
 *      Author: Lukasz
 */

#ifndef __TIM7_H_
#define __TIM7_H_

#define TIM7_SOURCECLOCK_FREQ ( 80000000u )
#define TIM7_TIMERCLOCK_FREQ  ( 1000000u )
#define TIM7_DELAY_9600BR     ( 4011u )

void TIM7_Initialize(void);
void TIM7_Start(void);
void TIM7_ResetCounter(void);

#endif /* __TIM7_H_ */
