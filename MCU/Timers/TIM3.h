/*
 * TIM3.h
 *
 *  Created on: 27 sie 2017
 *      Author: Lukasz
 */

#ifndef __TIM3_H_
#define __TIM3_H_

#define TIM3_SOURCECLOCK_FREQ ( 80000000u )
#define TIM3_TIMERCLOCK_FREQ  ( 80000000u )
#define TIM3_FREQ             ( 2500u )
#define TIM3_CC3_COMPARE      ( 0u )
#define TIM3_CC4_COMPARE      ( 0u )

void TIM3_Initialize(void);

void TIM3_EnableCounter(void);
void TIM3_DisableCounter(void);

#endif /* __TIM3_H_ */
