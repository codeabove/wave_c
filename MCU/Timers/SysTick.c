/*
 * SysTick.c
 *
 *  Created on: 04.04.2017
 *      Author: Lukasz
 */

#include "stdbool.h"
#include "stm32l4xx.h"

#include "MCU/ST/stm32l4xx_ll_cortex.h"
#include "MCU/ST/stm32l4xx_ll_utils.h"

#include "Drivers/LED/LEDDriver.h"

#include "SysTick.h"

#define SysTick_Ticks ( 1000u )

static volatile bool SysTick_1msPassed = false;
static volatile bool SysTick_10msPassed = false;
static volatile bool SysTick_100msPassed = false;
static volatile bool SysTick_500msPassed = false;
static volatile uint16_t SysTick_TimeCounter = 0u;

void SysTick_Initialize(void)
{
   /* Configure the SysTick to have interrupt in 1ms time base */
   SysTick->LOAD  = (uint32_t)((SystemCoreClock / SysTick_Ticks) - 1UL);  /* set reload register */
   SysTick->VAL   = 0UL;                                       /* Load the SysTick Counter Value */
   SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk;                  /* Configure clock source */

   /*** THIS FUNCTION DOES NOT ENABLE TIMER!!! ***/

}

void SysTick_Enable(void)
{
   LL_SYSTICK_EnableIT();
   NVIC_EnableIRQ(SysTick_IRQn);

   SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
}

extern void Logger_TimestampPerform(void);
void SysTick_Handler(void)
{
   SysTick_TimeCounter++;

   SysTick_1msPassed = true;

   if((SysTick_TimeCounter % 10u) == 0)
   {
      SysTick_10msPassed = true;
   }

   if((SysTick_TimeCounter % 100u) == 0)
   {
      LED_Toggle(LED_Yellow);
      SysTick_100msPassed = true;
   }

   if((SysTick_TimeCounter % 500u) == 0)
   {
      LED_Toggle(LED_Green);
      SysTick_500msPassed = true;
      SysTick_TimeCounter = 0u;
   }

   Logger_TimestampPerform();
}

bool SysTick_Is1msPassed(void)
{
   bool tmp = SysTick_1msPassed;
   SysTick_1msPassed = false;

   return tmp;
}

bool SysTick_Is10msPassed(void)
{
   bool tmp = SysTick_10msPassed;
   SysTick_10msPassed = false;

   return tmp;
}

bool SysTick_Is100msPassed(void)
{
   bool tmp = SysTick_100msPassed;
   SysTick_100msPassed = false;

   return tmp;
}
