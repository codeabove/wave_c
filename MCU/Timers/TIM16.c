/*
 * TIM16.c
 *
 *  Created on: 31 sie 2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_tim.h"

#include "TIM16.h"

void TIM16_Initialize(void)
{
   LL_TIM_SetClockSource(TIM16, LL_TIM_CLOCKSOURCE_INTERNAL);
   LL_TIM_SetPrescaler(TIM16, 0u);
   LL_TIM_SetAutoReload(TIM16, 0u);

   LL_TIM_OC_SetMode(TIM16, LL_TIM_CHANNEL_CH1, LL_TIM_OCMODE_PWM1);
   LL_TIM_OC_SetIdleState(TIM16, LL_TIM_CHANNEL_CH1, LL_TIM_OCIDLESTATE_LOW);
   LL_TIM_OC_SetPolarity(TIM16, LL_TIM_CHANNEL_CH1, LL_TIM_OCPOLARITY_HIGH);
   LL_TIM_OC_SetCompareCH1(TIM16, 0u);

   LL_TIM_CC_EnableChannel(TIM16, LL_TIM_CHANNEL_CH1);

   LL_TIM_EnableAllOutputs(TIM16);
}

void TIM16_EnableCounter(void)
{
   LL_TIM_EnableCounter(TIM16);
}

void TIM16_DisableCounter(void)
{
   LL_TIM_DisableCounter(TIM16);
}

void TIM16_SetPrescaler(uint16_t Prescaler)
{
   /* Prescaler-1 because counter clock frequency (CK_CNT) is equal to fCK_PSC / (PSC[15:0] + 1) */
   LL_TIM_SetPrescaler(TIM16, Prescaler-1);
}

void TIM16_SetAutoReloadRegister(uint16_t AutoReload)
{
   LL_TIM_SetAutoReload(TIM16, AutoReload);
}

void TIM16_SetOutputCompare(uint16_t Compare)
{
   LL_TIM_OC_SetCompareCH1(TIM16, Compare);
}
