/*
 * TIM1.h
 *
 *  Created on: 16 wrz 2017
 *      Author: Lukasz
 */

#ifndef __TIM1_H_
#define __TIM1_H_

void TIM1_Initialize(void);

void TIM1_EnableCounter(void);
void TIM1_DisableCounter(void);

#endif /* __TIM1_H_ */
