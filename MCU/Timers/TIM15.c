/*
 * TIM15.c
 *
 *  Created on: 24 sie 2017
 *      Author: Lukasz
 */


#include "MCU/ST/stm32l4xx_ll_tim.h"

#include "TIM15.h"

void TIM15_Initialize(void)
{
   LL_TIM_SetClockSource(TIM15, LL_TIM_CLOCKSOURCE_INTERNAL);
   LL_TIM_SetPrescaler(TIM15, __LL_TIM_CALC_PSC(TIM15_SOURCECLOCK_FREQ, TIM15_TIMERCLOCK_FREQ));
   LL_TIM_SetAutoReload(TIM15, __LL_TIM_CALC_ARR(TIM15_SOURCECLOCK_FREQ, LL_TIM_GetPrescaler(TIM15), TIM15_FREQ));

   LL_TIM_SetTriggerOutput(TIM15, LL_TIM_TRGO_UPDATE);

   NVIC_EnableIRQ(TIM1_BRK_TIM15_IRQn);
   LL_TIM_EnableIT_UPDATE(TIM15);
}

void TIM15_EnableCounter(void)
{
   LL_TIM_EnableCounter(TIM15);
}

void TIM15_DisableCounter(void)
{
   LL_TIM_DisableCounter(TIM15);
}

void TIM1_BRK_TIM15_IRQHandler(void)
{
   if (LL_TIM_IsActiveFlag_UPDATE(TIM15))
   {
      LL_TIM_ClearFlag_UPDATE(TIM15);
   }
}
