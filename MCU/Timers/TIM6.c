/*
 * TIM6.c
 *
 *  Created on: 7 lut 2018
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_tim.h"

#include "TIM6.h"
#include "Drivers/LED/LEDDriver.h"

void TIM6_Initialize(void)
{
   LL_TIM_SetOnePulseMode(TIM6, LL_TIM_ONEPULSEMODE_SINGLE);
   LL_TIM_SetPrescaler(TIM6, __LL_TIM_CALC_PSC(TIM6_SOURCECLOCK_FREQ, TIM6_TIMERCLOCK_FREQ));
   LL_TIM_SetAutoReload(TIM6, __LL_TIM_CALC_DELAY(TIM6_SOURCECLOCK_FREQ, LL_TIM_GetPrescaler(TIM6), TIM6_DELAY_9600BR));

   LL_TIM_SetUpdateSource(TIM6, LL_TIM_UPDATESOURCE_COUNTER);

   NVIC_EnableIRQ(TIM6_DAC_IRQn);
   LL_TIM_EnableIT_UPDATE(TIM6);
}

void TIM6_Start(void)
{
   LL_TIM_EnableCounter(TIM6);
}

void TIM6_ResetCounter(void)
{
   LL_TIM_GenerateEvent_UPDATE(TIM6);
}

extern void ModbusRTU_TimerT15Callback(void);
void TIM6_DAC_IRQHandler(void)
{
   if (LL_TIM_IsActiveFlag_UPDATE(TIM6))
   {
      ModbusRTU_TimerT15Callback();
      LL_TIM_ClearFlag_UPDATE(TIM6);
   }
}
