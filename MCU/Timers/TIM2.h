/*
 * TIM2.h
 *
 *  Created on: 30.03.2017
 *      Author: Lukasz
 */

#ifndef __TIM2_H
#define __TIM2_H

#define TIM2_SOURCECLOCK_FREQ ( 80000000u )
#define TIM2_TIMERCLOCK_FREQ  (  1000000u )
#define TIM2_FREQ             (     1000u )
#define TIM2_CC1_COMPARE      (      100u )

void TIM2_Initialize(void);

void TIM2_EnableCounter(void);
void TIM2_DisableCounter(void);

#endif /* __TIM2_H */
