/*
 * TIM6.h
 *
 *  Created on: 7 lut 2018
 *      Author: Lukasz
 */

#ifndef __TIM6_H_
#define __TIM6_H_

#define TIM6_SOURCECLOCK_FREQ ( 80000000u )
#define TIM6_TIMERCLOCK_FREQ  ( 1000000u )
#define TIM6_DELAY_9600BR     ( 1719u )

void TIM6_Initialize(void);
void TIM6_Start(void);
void TIM6_ResetCounter(void);

#endif /* __TIM6_H_ */
