/*
 * TIM15.h
 *
 *  Created on: 24 sie 2017
 *      Author: Lukasz
 */

#ifndef __TIM15_H_
#define __TIM15_H_

#define TIM15_SOURCECLOCK_FREQ ( 80000000u )
#define TIM15_TIMERCLOCK_FREQ  ( 10000u )
#define TIM15_FREQ             ( 10u )

void TIM15_Initialize(void);

void TIM15_EnableCounter(void);
void TIM15_DisableCounter(void);

#endif /* __TIM15_H_ */
