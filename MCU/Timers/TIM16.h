/*
 * TIM16.h
 *
 *  Created on: 31 sie 2017
 *      Author: Lukasz
 */

#ifndef __TIM16_H_
#define __TIM16_H_

void TIM16_Initialize(void);

void TIM16_EnableCounter(void);
void TIM16_DisableCounter(void);

void TIM16_SetPrescaler(uint16_t Prescaler);
void TIM16_SetAutoReloadRegister(uint16_t AutoReload);
void TIM16_SetOutputCompare(uint16_t Compare);

#endif /* __TIM16_H_ */
