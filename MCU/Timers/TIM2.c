/*
 * TIM2.c
 *
 *  Created on: 30.03.2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_tim.h"

#include "TIM2.h"

void TIM2_Initialize(void)
{
   LL_TIM_SetClockSource(TIM2, LL_TIM_CLOCKSOURCE_INTERNAL);
   LL_TIM_SetPrescaler(TIM2, __LL_TIM_CALC_PSC(TIM2_SOURCECLOCK_FREQ, TIM2_TIMERCLOCK_FREQ));
   LL_TIM_SetAutoReload(TIM2, __LL_TIM_CALC_ARR(TIM2_SOURCECLOCK_FREQ, LL_TIM_GetPrescaler(TIM2), TIM2_FREQ));

   LL_TIM_SetOnePulseMode(TIM2, LL_TIM_ONEPULSEMODE_SINGLE);

   LL_TIM_OC_SetMode(TIM2, LL_TIM_CHANNEL_CH1, LL_TIM_OCMODE_TOGGLE);
   LL_TIM_OC_SetIdleState(TIM2, LL_TIM_CHANNEL_CH1, LL_TIM_OCIDLESTATE_LOW);
   LL_TIM_OC_SetPolarity(TIM2, LL_TIM_CHANNEL_CH1, LL_TIM_OCPOLARITY_HIGH);
   LL_TIM_OC_SetCompareCH1(TIM2, TIM2_CC1_COMPARE);

   LL_TIM_CC_EnableChannel(TIM2, LL_TIM_CHANNEL_CH1);

   NVIC_EnableIRQ(TIM2_IRQn);
   LL_TIM_EnableIT_CC1(TIM2);
}

void TIM2_EnableCounter(void)
{
   LL_TIM_EnableCounter(TIM2);
}

void TIM2_DisableCounter(void)
{
   LL_TIM_DisableCounter(TIM2);
}

void TIM2_IRQHandler(void)
{
   if(LL_TIM_IsActiveFlag_UPDATE(TIM2))
   {
      LL_TIM_ClearFlag_UPDATE(TIM2);
   }
   else if (LL_TIM_IsActiveFlag_CC1(TIM2))
   {
      LL_TIM_ClearFlag_CC1(TIM2);
   }
}


