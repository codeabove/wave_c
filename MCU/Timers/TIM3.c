/*
 * TIM3.c
 *
 *  Created on: 27 sie 2017
 *      Author: Lukasz
 */

#include "MCU/ST/stm32l4xx_ll_tim.h"

#include "TIM3.h"

/*!
 * Practical notes:
 * For maximal rotary speed of motor there
 * should be 2 to 5 PWM pulses per round.
 *
 * For example:
 *    Pololu 30:1 Micro Metal Gearmotor HP 6V
 *    Speed [RPM]:   1000
 *    Gear ratio:    30:1
 *
 *       1000[RPM] * 30 = 30000[RPM]
 *       30000[RPM] / 60 = 500[Hz]
 *       500[Hz] * 2 = 1000[Hz]
 *       500[Hz] * 5 = 2500[Hz]
 *
 *    PWM frequency [Hz]: 1000 - 2500
 */

void TIM3_Initialize(void)
{
   LL_TIM_SetClockSource(TIM3, LL_TIM_CLOCKSOURCE_INTERNAL);
   LL_TIM_SetPrescaler(TIM3, __LL_TIM_CALC_PSC(TIM3_SOURCECLOCK_FREQ, TIM3_TIMERCLOCK_FREQ));
   LL_TIM_SetAutoReload(TIM3, __LL_TIM_CALC_ARR(TIM3_SOURCECLOCK_FREQ, LL_TIM_GetPrescaler(TIM3), TIM3_FREQ));

   LL_TIM_OC_SetMode(TIM3, LL_TIM_CHANNEL_CH3, LL_TIM_OCMODE_PWM1);
   LL_TIM_OC_SetIdleState(TIM3, LL_TIM_CHANNEL_CH3, LL_TIM_OCIDLESTATE_LOW);
   LL_TIM_OC_SetPolarity(TIM3, LL_TIM_CHANNEL_CH3, LL_TIM_OCPOLARITY_HIGH);
   LL_TIM_OC_SetMode(TIM3, LL_TIM_CHANNEL_CH4, LL_TIM_OCMODE_PWM1);
   LL_TIM_OC_SetIdleState(TIM3, LL_TIM_CHANNEL_CH4, LL_TIM_OCIDLESTATE_LOW);
   LL_TIM_OC_SetPolarity(TIM3, LL_TIM_CHANNEL_CH4, LL_TIM_OCPOLARITY_HIGH);
   LL_TIM_OC_SetCompareCH3(TIM3, TIM3_CC3_COMPARE);
   LL_TIM_OC_SetCompareCH4(TIM3, TIM3_CC4_COMPARE);

   LL_TIM_CC_EnableChannel(TIM3, LL_TIM_CHANNEL_CH3);
   LL_TIM_CC_EnableChannel(TIM3, LL_TIM_CHANNEL_CH4);
}

void TIM3_EnableCounter(void)
{
   LL_TIM_EnableCounter(TIM3);
}

void TIM3_DisableCounter(void)
{
   LL_TIM_DisableCounter(TIM3);
}
