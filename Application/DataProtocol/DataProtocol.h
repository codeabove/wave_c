/*
 * DataProtocol.h
 *
 *  Created on: 13 lis 2017
 *      Author: Lukasz
 */

#ifndef __DataProtocol_H_
#define __DataProtocol_H_

#include "stdbool.h"
#include "stdint.h"

#define DataProtocol_DataMaxLength ( 4u )

typedef enum DataProtocol_FrameId
{
   DataProtocol_FrameId_Acknowledge = 0u,
   DataProtocol_FrameId_NotAcknowledge,
   DataProtocol_FrameId_Get,
   DataProtocol_FrameId_Set,

   DataProtocol_FrameId_BatteryLevel,

   DataProtocol_FrameId_DataStream,
   /* Keep this last */
   DataProtocol_FrameId_Invalid,
} DataProtocol_FrameId_T;

typedef enum DataProtocol_DataType
{
   DataProtocol_DataType_BatteryLevel = 0u,

   /* Keep this last */
   DataProtocol_DataType_Invalid
} DataProtocol_DataType_T;

typedef struct DataProtocol_Message
{
   DataProtocol_FrameId_T ID;
   DataProtocol_FrameId_T DataType;

   uint8_t Data[DataProtocol_DataMaxLength];

} DataProtocol_Message_T;

void DataProtocol_Perform(void);

bool DataProtocol_IsNewMessageReady(void);
DataProtocol_Message_T DataProtocol_GetNewMessage(void);
void DataProtocol_SendMessage(DataProtocol_Message_T Message);

#endif /* __DataProtocol_H_ */
