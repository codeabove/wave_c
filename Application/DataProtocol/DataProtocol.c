/*
 * DataProtocol.c
 *
 *  Created on: 13 lis 2017
 *      Author: Lukasz
 */

#include "stdint.h"
#include "stdio.h"
#include "string.h"

#include "DataProtocol.h"

#include "Drivers/HC08/HC08.h"
#include "Framework/Logger/Logger.h"

#define DataProtocol_StartOfFrame  ( '$' )
#define DataProtocol_EndOfFrame    ( '#' )

#define DataProtocol_CRCMask       ( 0xFFu )

typedef enum DataProtocol_State
{
   DataProtocol_State_WaitForStart = 0u,
   DataProtocol_State_ParseId,
   DataProtocol_State_ParseData,
   DataProtocol_State_ParseCrc,
   DataProtocol_State_WaitForEnd
} DataProtocol_State_T;

typedef struct DataProtocol_Frame
{
   uint8_t ID;
   uint8_t Data[DataProtocol_DataMaxLength];
   uint8_t CRC;
} DataProtocol_Frame_T;

typedef struct DataProtocol
{
   DataProtocol_State_T CurrentState;
   DataProtocol_Frame_T IncomingFrame;
   uint8_t IncomingDataCount;

   bool NewMessageReady;
   DataProtocol_Message_T NewMessage;

} DataProtocol_T;

static void DataProtocol_privWaitForStart(void);
static void DataProtocol_privParseId(void);
static void DataProtocol_privParseData(void);
static void DataProtocol_privParseCrc(void);
static void DataProtocol_privWaitForEnd(void);

static inline bool DataProtocol_IsNewDataReady(void);
static inline uint8_t DataProtocol_GetNewData(void);
static void DataProtocol_InitializeFrame(DataProtocol_Frame_T *Frame);
static uint8_t DataProtocol_privCalculateCRC(DataProtocol_Frame_T *Frame);
static void DataProtocol_AcknowledgeFrame(DataProtocol_FrameId_T FrameID);
static void DataProtocol_NotAcknowledgeFrame(DataProtocol_FrameId_T FrameID);
static void DataProtocol_privSendFrame(DataProtocol_Frame_T Frame);
static void DataProtocol_LogFrame(DataProtocol_Frame_T Frame);
static void DataProtocol_ClearMessage(DataProtocol_Message_T *Message);

static DataProtocol_T DataProtocol =
{
      .CurrentState = DataProtocol_State_WaitForStart,
      .IncomingFrame =
      {
            .ID = DataProtocol_FrameId_Invalid,
            .Data = { 0u },
            .CRC = 0u
      },
      .IncomingDataCount = 0u,

      .NewMessageReady = false,
      .NewMessage =
      {
            .ID = DataProtocol_FrameId_Invalid,
            .DataType = DataProtocol_FrameId_Invalid,
            .Data = { 0u }
      }
};

void DataProtocol_Perform(void)
{
   switch(DataProtocol.CurrentState)
   {
   case DataProtocol_State_WaitForStart:
      DataProtocol_privWaitForStart();
      break;
   case DataProtocol_State_ParseId:
      DataProtocol_privParseId();
      break;
   case DataProtocol_State_ParseData:
      DataProtocol_privParseData();
      break;
   case DataProtocol_State_ParseCrc:
      DataProtocol_privParseCrc();
      break;
   case DataProtocol_State_WaitForEnd:
      DataProtocol_privWaitForEnd();
      break;
   default:
      /* Should not get here, let's reset */
      DataProtocol.CurrentState = DataProtocol_State_WaitForStart;
      break;
   }
}

bool DataProtocol_IsNewMessageReady(void)
{
   return DataProtocol.NewMessageReady;
}

DataProtocol_Message_T DataProtocol_GetNewMessage(void)
{
   DataProtocol_Message_T Message = DataProtocol.NewMessage;

   DataProtocol_ClearMessage(&DataProtocol.NewMessage);
   DataProtocol.NewMessageReady = false;

   return Message;
}

void DataProtocol_SendMessage(DataProtocol_Message_T Message)
{
   DataProtocol_Frame_T Frame;
   DataProtocol_InitializeFrame(&Frame);

   Frame.ID = Message.ID;
   memcpy(Frame.Data, Message.Data, DataProtocol_DataMaxLength);
   Frame.CRC = DataProtocol_privCalculateCRC(&Frame);

   DataProtocol_privSendFrame(Frame);
}

static void DataProtocol_privWaitForStart(void)
{
   uint8_t Data;

   if(DataProtocol_IsNewDataReady() == true)
   {
      Data = DataProtocol_GetNewData();

      if(Data == DataProtocol_StartOfFrame)
      {
         DataProtocol.IncomingDataCount = 0u;

         /* Received SOF character, reset frame */
         DataProtocol_InitializeFrame(&DataProtocol.IncomingFrame);

         DataProtocol.CurrentState = DataProtocol_State_ParseId;
      }
   }
}

static void DataProtocol_privParseId(void)
{
   uint8_t Data;

   if(DataProtocol_IsNewDataReady() == true)
   {
      Data = DataProtocol_GetNewData();

      DataProtocol.IncomingFrame.ID = (DataProtocol_FrameId_T)Data;
      if(DataProtocol.IncomingFrame.ID < DataProtocol_FrameId_Invalid)
      {
         /* ID is correct, proceed */
         DataProtocol.CurrentState = DataProtocol_State_ParseData;
      }
      else
      {
         /* ID is not correct, NACK and reset */
         //TODO: NACK here
         DataProtocol.CurrentState = DataProtocol_State_WaitForStart;
      }
   }
}

static void DataProtocol_privParseData(void)
{
   uint8_t Data;

   if(DataProtocol_IsNewDataReady() == true)
   {
      Data = DataProtocol_GetNewData();

      DataProtocol.IncomingFrame.Data[DataProtocol.IncomingDataCount] = Data;

      DataProtocol.IncomingDataCount++;
      if(DataProtocol.IncomingDataCount == DataProtocol_DataMaxLength)
      {
         /* Data collected, proceed to next step */
         DataProtocol.CurrentState = DataProtocol_State_ParseCrc;
      }
   }
}

static void DataProtocol_privParseCrc(void)
{
   uint8_t Data;

   if(DataProtocol_IsNewDataReady() == true)
   {
      Data = DataProtocol_GetNewData();

      DataProtocol.IncomingFrame.CRC = Data;

      DataProtocol.CurrentState = DataProtocol_State_WaitForEnd;
   }
}

static void DataProtocol_privWaitForEnd(void)
{
   uint8_t Data;
   uint8_t CRC;

   if(DataProtocol_IsNewDataReady() == true)
   {
      Data = DataProtocol_GetNewData();

      if(Data == DataProtocol_EndOfFrame)
      {
         CRC = DataProtocol_privCalculateCRC(&DataProtocol.IncomingFrame);

         if(CRC == DataProtocol.IncomingFrame.CRC)
         {
            /* All fine, acknowledge frame (if it's not ACK or NACK) */
            if(   DataProtocol.IncomingFrame.ID == DataProtocol_FrameId_Acknowledge
               || DataProtocol.IncomingFrame.ID == DataProtocol_FrameId_NotAcknowledge)
            {
               //TODO: Do something on ACK/NACK
            }
            else
            {
//               DataProtocol_AcknowledgeFrame(DataProtocol.IncomingFrame.ID);

               DataProtocol.NewMessageReady = true;
               DataProtocol.NewMessage.ID = DataProtocol.IncomingFrame.ID;
               // TODO: Sticky
               DataProtocol.NewMessage.DataType = DataProtocol.IncomingFrame.Data[DataProtocol_DataMaxLength-1];
            }
         }
         else
         {
            /* Something is wrong with frame, not acknowledge it (if it's not ACK or NACK) */
//            DataProtocol_NotAcknowledgeFrame(DataProtocol.IncomingFrame.ID);
         }
      }
      else
      {
         /* Something went wrong, not acknowledge */
//         DataProtocol_NotAcknowledgeFrame(DataProtocol.IncomingFrame.ID);
      }

      DataProtocol.CurrentState = DataProtocol_State_WaitForStart;

      DataProtocol_LogFrame(DataProtocol.IncomingFrame);
   }
}

static void DataProtocol_InitializeFrame(DataProtocol_Frame_T *Frame)
{
   Frame->ID = DataProtocol_FrameId_Invalid;
   Frame->CRC = 0u;
   memset(Frame->Data, 0u, DataProtocol_DataMaxLength);
}

static uint8_t DataProtocol_privCalculateCRC(DataProtocol_Frame_T *Frame)
{
   uint8_t i;
   uint32_t CRC = 0u;

   CRC += Frame->ID;
   for(i = 0u; i < DataProtocol_DataMaxLength; i++)
   {
      CRC += Frame->Data[i];
   }

   return (uint8_t)(CRC & DataProtocol_CRCMask);
}

static void DataProtocol_AcknowledgeFrame(DataProtocol_FrameId_T FrameID)
{
   DataProtocol_Frame_T Frame;

   Frame.ID = DataProtocol_FrameId_Acknowledge;
   Frame.Data[DataProtocol_DataMaxLength-1] = FrameID; //TODO: More flexible?
   Frame.CRC = DataProtocol_privCalculateCRC(&Frame);

   DataProtocol_privSendFrame(Frame);
}

static void DataProtocol_NotAcknowledgeFrame(DataProtocol_FrameId_T FrameID)
{
   DataProtocol_Frame_T Frame;

   Frame.ID = DataProtocol_FrameId_NotAcknowledge;
   Frame.Data[DataProtocol_DataMaxLength-1] = FrameID; //TODO: More flexible?
   Frame.CRC = DataProtocol_privCalculateCRC(&Frame);

   DataProtocol_privSendFrame(Frame);
}

static void DataProtocol_privSendFrame(DataProtocol_Frame_T Frame)
{
   uint8_t i;

   HC08_SendBinary(DataProtocol_StartOfFrame);

   HC08_SendBinary(Frame.ID);
   for(i = 0; i < DataProtocol_DataMaxLength; i++)
   {
      HC08_SendBinary(Frame.Data[i]);
   }
   HC08_SendBinary(Frame.CRC);

   HC08_SendBinary(DataProtocol_EndOfFrame);

   DataProtocol_LogFrame(Frame);
}

static inline bool DataProtocol_IsNewDataReady(void)
{
   return HC08_IsNewDataReceived();
}

static inline uint8_t DataProtocol_GetNewData(void)
{
   return HC08_GetNewData();
}

static void DataProtocol_LogFrame(DataProtocol_Frame_T Frame)
{
   uint8_t Buffer[35];

   sprintf(
         Buffer,
         "[RX]<$><%.2x><%.2x><%.2x><%.2x><%.2x><%.2x><#>\n",
         Frame.ID,
         Frame.Data[0],
         Frame.Data[1],
         Frame.Data[2],
         Frame.Data[3],
         Frame.CRC
         );

   Logger_SendText(Buffer, true);
}

static void DataProtocol_ClearMessage(DataProtocol_Message_T *Message)
{
   Message->ID = DataProtocol_FrameId_Invalid;
   Message->DataType = DataProtocol_FrameId_Invalid;
   memset(Message->Data, 0u, DataProtocol_DataMaxLength);
}
