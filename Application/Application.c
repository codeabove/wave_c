/*
 * Application.c
 *
 *  Created on: 04.03.2017
 *      Author: Lukasz
 */

#include "stm32l4xx.h"
#include "Application.h"

#include "MCU/MCU.h"
#include "MCU/Communication/SPI3.h"
#include "MCU/Timers/TIM1.h"
#include "MCU/Timers/TIM3.h"
#include "MCU/Timers/TIM4.h"
#include "MCU/Timers/TIM6.h"
#include "MCU/Timers/TIM7.h"
#include "MCU/Timers/TIM8.h"
#include "MCU/Timers/TIM15.h"
#include "MCU/Timers/TIM16.h"
#include "MCU/Timers/SysTick.h"

#include "MCU/ST/stm32l4xx_ll_adc.h"
#include "MCU/ST/stm32l4xx_ll_dma.h"
#include "MCU/ST/stm32l4xx_ll_gpio.h"
#include "MCU/ST/stm32l4xx_ll_spi.h"
#include "MCU/ST/stm32l4xx_ll_tim.h"
#include "MCU/ST/stm32l4xx_ll_usart.h"
#include "MCU/ST/stm32l4xx_ll_utils.h"

#include "Drivers/BatteryMeter/BatteryMeterDriver.h"
#include "Drivers/Button/ButtonDriver.h"
#include "Drivers/Encoder/EncoderDriver.h"
#include "Drivers/HC08/HC08.h"
#include "Drivers/L3GD20H/L3GD20H.h"
#include "Drivers/LED/LEDDriver.h"
#include "Drivers/MotorDriver/MotorDriver.h"
#include "Drivers/Proximity/ProximityDriver.h"
#include "Drivers/TB6612FNG/TB6612FNG.h"

#include "Framework/Logger/Logger.h"
#include "Framework/SoundPlayer/Melody.h"
#include "Framework/PID/PID.h"
#include "Framework/SoundPlayer/SoundPlayer.h"

#include "Application/DataProtocol/DataProtocol.h"
#include "Application/Managers/CommunicationManager/CommunicationManager.h"
#include "Application/Modbus/ModbusSlave.h"
#include "Application/Modbus/ModbusRTU.h"

static void Application_privTestLoop(void);

#if 0
static void Application_privInitializeLowLevelDrivers(void);
static void Application_privInitializeFrameworks(void);
static void Application_privInitializeHighLevelLayers(void);
static void Application_privEnableInterruptsAndPeripherals(void);

static void Application_PerformFakeEncoder(void);
#endif

static float DesiredSpeed = 0.0f;
static int32_t DesiredSpeedInt = 0;
static bool LeftPressed = false;
static bool RightPressed = false;
static PID_T TestPID =
{
      .GainP = 0.5f,
      .GainI = 0.1f,
      .GainD = 0.0f,
      .IterationTime = 0.001f,
      .OutputMin = -1000.0f,
      .OutputMax = 1000.0f
};

uint8_t Text_100ms[6] = { '1', '0', '0', 'm', 's', '\n' };

/*!
 * \brief      Main application entry point
 *
 * @param[in]  void
 * @param[out] void
 */
void Application_Main(void)
{
   /*
    * 0. Enable MCU debug mode
    * 1. Initialize MCU peripheral drivers
    * 2. Initialize logger
    * 3. Enable SysTick timer
    * 4. Execute Wave application main state machine
    * *  4.1. Initialize external peripheral drivers
    * *  4.2. Initialize frameworks
    * *  4.3. Initialize high level layers
    * *  4.4. Enable interrupts and peripherals
    */
   MCU_EnableDebugFeatures();
   MCU_Initialize();

   Logger_Initialize();
   SysTick_Enable();

   //TODO: Remove when no needed
   Application_privTestLoop();

   //TODO: Enter Wave main state machine here
}

static void Application_privTestLoop(void)
{
   uint8_t string[] = "Wave\n";

   ////Battery meter
   LL_ADC_REG_StartConversion(ADC2);
   TIM15_EnableCounter();

   ////Motor driver
   TIM3_EnableCounter();
   TB6612FNG_DisableStandby();

   ////Buzzer
   TIM16_EnableCounter();

   ////Encoders (right)
   TIM1_EnableCounter();
   TIM8_EnableCounter();

   ////Proximity
   ProximityDriver_Initialize();
   TIM4_EnableCounter();

   ////Test PID
   PID_Initialize(&TestPID);

   ////Bluetooth
   HC08_Initialize();

   ////Modbus
   ModbusSlave_Init();

   Logger_SendText(string, false);

   while(1)
   {
      BatteryMeter_Perform();
      ModbusSlave_Perform();
//      Application_PerformFakeEncoder();

      if(SysTick_Is1msPassed() == true)
      {
//         MotorDriver_Perform(Motor_Left);


      }

      if(SysTick_Is10msPassed() == true)
      {

      }

      if(SysTick_Is100msPassed() == true)
      {
         EncoderDriver_Perform();
//         if((Button_GetState(Button_Left) != 0) && (LeftPressed == false))
//         {
//            DesiredSpeed -= 100.0f;
//            if(DesiredSpeed < 0.0f) DesiredSpeed = 0.0f;
//            LeftPressed = true;
//         }
//         else if((Button_GetState(Button_Right) != 0) && (RightPressed == false))
//         {
//            DesiredSpeed += 100.0f;
//            if(DesiredSpeed > 1000.0f) DesiredSpeed = 1000.0f;
//            RightPressed = true;
//         }
//         else
//         {
//            LeftPressed = false;
//            RightPressed = false;
//         }
//
//         DesiredSpeedInt = (int16_t)DesiredSpeed;
//         MotorDriver_SetSpeed(Motor_Left, DesiredSpeedInt);

//         LED_Toggle(LED_Red);
         SoundPlayer_Perform();
      }
   }
}

#if 0
void Application_PerformFakeEncoder(void)
{
   static uint8_t i = 0;

   switch(i)
   {
   case 0:
      LED_Off(LED_Red);
      LED_Off(LED_Orange);
      break;
   case 1:
      LED_On(LED_Red);
      LED_Off(LED_Orange);
      break;
   case 2:
      LED_On(LED_Red);
      LED_On(LED_Orange);
      break;
   case 3:
      LED_Off(LED_Red);
      LED_On(LED_Orange);
      break;
   default:
      break;
   }

   i++;
   if(i == 4) i = 0;
}

void Application_privSystemInit(void)
{
   /*
    * 0. Enable MCU debug mode
    * 1. Initialize MCU
    * 2. Initialize low level drivers
    * 3. Initialize frameworks
    * 4. Initialize high level layers
    * 5. Enable interrupts and peripherals
    */




   Application_privInitializeLowLevelDrivers();
   Application_privInitializeFrameworks();
   Application_privInitializeHighLevelLayers();
   Application_privEnableInterruptsAndPeripherals();
}

static void Application_privInitializeLowLevelDrivers(void)
{
   ProximityDriver_Initialize();
   L3GD20H_Initialize();
}

static void Application_privInitializeFrameworks(void)
{

}

static void Application_privInitializeHighLevelLayers(void)
{

}

static void Application_privEnableInterruptsAndPeripherals(void)
{

   //TODO: Replace this
   TIM4_EnableCounter();
   LL_USART_EnableIT_TXE(USART3);

   //SPI3_Enable();
}
#endif
