/*
 * CommunicationManager.h
 *
 *  Created on: 4 gru 2017
 *      Author: Lukasz
 */

#ifndef __CommunicationManager_H_
#define __CommunicationManager_H_

void CommunicationManager_Perform(void);
void CommunicationProtocol_Perform(void);

#endif /* __CommunicationManager_H_ */
