/*
 * CommunicationManager.c
 *
 *  Created on: 4 gru 2017
 *      Author: Lukasz
 */

#include "CommunicationManager.h"

#include "Application/DataProtocol/DataProtocol.h"

#include "Drivers/BatteryMeter/BatteryMeterDriver.h"

static void CommunicationProtocol_ProvideData(DataProtocol_FrameId_T DataType);
static void CommunicationProtocol_PackData(uint32_t Data, uint8_t *Buffer, uint8_t Size);

void CommunicationProtocol_Perform(void)
{
   DataProtocol_Message_T Message;

   if(DataProtocol_IsNewMessageReady() == true)
   {
      Message = DataProtocol_GetNewMessage();

      if(Message.ID == DataProtocol_FrameId_Get)
      {
         CommunicationProtocol_ProvideData(Message.DataType);
      }
   }
}

static void CommunicationProtocol_ProvideData(DataProtocol_FrameId_T DataType)
{
   uint32_t Data;
   DataProtocol_Message_T Message;

   switch(DataType)
   {
   case DataProtocol_FrameId_BatteryLevel:
      Data = BatteryMeter_GetBatteryVoltage();

      Message.ID = DataProtocol_FrameId_BatteryLevel;
      CommunicationProtocol_PackData(Data, Message.Data, DataProtocol_DataMaxLength);
      break;
   default:
      break;
   }

   DataProtocol_SendMessage(Message);
}

static void CommunicationProtocol_PackData(uint32_t Data, uint8_t *Buffer, uint8_t Size)
{
   uint8_t i;

   memcpy(Buffer, &Data, Size);
}
