/*
 * CRC.h
 *
 *  Created on: 11 lut 2018
 *      Author: Lukasz
 */

#ifndef __CRC_H_
#define __CRC_H_

#include "stdint.h"

uint16_t CRC16(uint8_t * puchMsg, uint16_t usDataLen);

#endif /* __CRC_H_ */
