/*
 * ModbusRTU.h
 *
 *  Created on: 6 lut 2018
 *      Author: Lukasz
 */

#ifndef __ModbusRTU_H_
#define __ModbusRTU_H_

#include "ModbusCommon.h"
#include "Framework/FIFO/FIFO.h"

void ModbusRTU_Init(void);
void ModbusRTU_TransmitResponse(ModbusResponse_T *Response);
void ModbusRTU_TransmitError(ModbusError_T *Error);

FIFO_Buffer_T* ModbusRTU_GetRxBuffer(void);

#endif /* __ModbusRTU_H_ */
