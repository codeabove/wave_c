/*
 * ModbusDataUsr.h
 *
 *  Created on: 17 mar 2018
 *      Author: Lukasz
 */

#ifndef __ModbusDataUsr_H_
#define __ModbusDataUsr_H_

#define ModbusData_HoldingRegistersQty ( 11u )

typedef uint16_t (*RefreshFunction)(void);
typedef void (*SetFunction)(uint16_t);

void ModbusDataUsr_InitHoldingRegisterRefreshFunctions(RefreshFunction *HoldingRegistersRefreshFunctions);
void ModbusDataUsr_InitHoldingRegisterSetFunctions(SetFunction *HoldingRegistersSetFunctions);

#endif /* __ModbusDataUsr_H_ */
