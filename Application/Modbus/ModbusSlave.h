/*
 * ModbusSlave.h
 *
 *  Created on: 12 lut 2018
 *      Author: Lukasz
 */

#ifndef __ModbusSlave_H_
#define __ModbusSlave_H_

void ModbusSlave_Init(void);
void ModbusSlave_Perform(void);

void ModbusSlave_ReceivedRequestCallback(void);
void ModbusSlave_NormalReplySentCallback(void);
void ModbusSlave_ErrorReplySentCallback(void);

#endif /* __ModbusSlave_H_ */
