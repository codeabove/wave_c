/*
 * ModbusData.c
 *
 *  Created on: 21 lut 2018
 *      Author: Lukasz
 */

#include "stdint.h"
#include "string.h"
#include "ModbusData.h"
#include "ModbusDataUsr.h"
#include "ModbusUtils.h"

typedef struct ModbusData
{
   uint16_t HoldingRegisters[ModbusData_HoldingRegistersQty];
   RefreshFunction HoldingRegistersRefreshFunctions[ModbusData_HoldingRegistersQty];
   SetFunction HoldingRegistersSetFunctions[ModbusData_HoldingRegistersQty];
} ModbusData_T;

static ModbusData_T ModbusData;

void ModbusData_Init(void)
{
   memset(ModbusData.HoldingRegisters, 0, ModbusData_HoldingRegistersQty*2);
   ModbusDataUsr_InitHoldingRegisterRefreshFunctions(ModbusData.HoldingRegistersRefreshFunctions);
   ModbusDataUsr_InitHoldingRegisterSetFunctions(ModbusData.HoldingRegistersSetFunctions);
}

void ModbusData_RefreshHoldingRegisters(uint16_t StartAddress, uint16_t EndAddress)
{
   uint16_t i;

   for(i = StartAddress; i < EndAddress; i++)
   {
      if(NULL != ModbusData.HoldingRegistersRefreshFunctions[i])
      {
         ModbusData.HoldingRegisters[i] = ModbusData.HoldingRegistersRefreshFunctions[i]();
      }
   }
}

void ModbusData_WriteHoldingRegisters(uint16_t StartAddress, uint16_t RegistersQuantity, uint8_t* Buffer)
{
   uint16_t i;
   uint16_t tmp;

   for(i = 0; i < RegistersQuantity; i++)
   {
      tmp = 0u;

      ModbusUtils_SetHighByte(&tmp, Buffer[2*i]);
      ModbusUtils_SetLowByte(&tmp, Buffer[2*i+1]);

      if(NULL != ModbusData.HoldingRegistersSetFunctions[StartAddress+i])
      {
         ModbusData.HoldingRegisters[StartAddress+i] = tmp;
         ModbusData.HoldingRegistersSetFunctions[StartAddress+i](tmp);
      }
   }
}

uint16_t* ModbusData_GetHoldingRegisters(void)
{
   return ModbusData.HoldingRegisters;
}
