/*
 * ModbusSlave.c
 *
 *  Created on: 12 lut 2018
 *      Author: Lukasz
 */

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"

#include "IModbus.h"
#include "ModbusCommon.h"
#include "ModbusData.h"
#include "ModbusDataUsr.h"
#include "ModbusRTU.h"
#include "ModbusSlave.h"
#include "ModbusUtils.h"

#define ReadHoldingRegisters_MinRegQty ( 1u )
#define ReadHoldingRegisters_MaxRegQty ( 125u )
#define ReadHOldingRegisters_ByteCountSize ( 1u )

#define WriteMultipleRegisters_MinReqQty ( 1u )
#define WriteMultipleRegisters_MaxReqQty ( 123u )
#define WriteMultipleRegisters_ByteCountField ( 6u )
#define WriteMultipleRegisters_DataSize ( 4u )

typedef enum ModbusSlaveEvent
{
   SlaveEvent_NoEvent = 0u,
   SlaveEvent_ReceivedRequest,
   SlaveEvent_NormalReplySent,
   SlaveEvent_ErrorReplySent
} ModbusSlaveEvent_T;

typedef enum SlaveState
{
   SlaveState_Init = 0u, // Custom state to force Init function call
   SlaveState_Idle,
   SlaveState_CheckingRequest,
   SlaveState_ProcessingRequiredAction,
   SlaveState_FormattingNormalReply,
   SlaveState_FormattingErrorReply
} SlaveState_T;

typedef struct ModbusSlave
{
   FIFO_Buffer_T *RxBuffer;
   volatile SlaveState_T State;

   uint8_t Exception;
   bool IsProcessingActionSuccessful;
   ModbusRequest_T Request;
   ModbusResponse_T Response;
   ModbusError_T Error;

   volatile ModbusSlaveEvent_T Event;

   bool ReplyReady;
} ModbusSlave_T;

static void ProcessRequest(void);
static void BuildNormalReply(void);
static void BuildErrorReply(void);
static bool IsRequestCorrect(void);
static bool IsUnicastMode(void);
static uint8_t GetFrameFunctionCodeField(void);
static uint8_t GetFrameByteCountField(void);
static uint16_t GetFrameRegistersQuantityField(void);
static uint16_t GetFrameStartingAddressField(void);
static uint8_t* GetFrameRegistersValueAddress(void);
static inline void SetEvent(ModbusSlaveEvent_T Event);

static ModbusSlave_T Slave =
{
      .State = SlaveState_Init,
      .Event = SlaveEvent_NoEvent,

      .ReplyReady = false
};

void ModbusSlave_Init(void)
{
   ModbusRTU_Init();
   ModbusData_Init();

   Slave.RxBuffer = ModbusRTU_GetRxBuffer();
   Slave.State = SlaveState_Idle;
}

void ModbusSlave_Perform(void)
{
   ModbusSlaveEvent_T CurrentEvent;

   ModbusSlave_DisableCommunicationInterrupts();
   ModbusSlave_DisableT35Interrupts();
   /* Slave event can be overwritten by call from RTU within interrupt.
    * Copy the state to variable to prevent sudden changes and weird behaviour. */
   CurrentEvent = Slave.Event;

   /* Clear event, it will be processed now from variable
    * and there is no need to keep it here. Also this can
    * be written with data coming within ISR. */
   SetEvent(SlaveEvent_NoEvent);
   ModbusSlave_EnableCommunicationInterrupts();
   ModbusSlave_EnableT35Interrupts();

   switch(Slave.State)
   {
      case SlaveState_Idle:
         if(SlaveEvent_ReceivedRequest == CurrentEvent)
         {
            Slave.Exception = 0u;
            Slave.State = SlaveState_CheckingRequest;
         }
         else { /* Do nothing */ }
         break;

      case SlaveState_CheckingRequest:
         if(IsRequestCorrect() == true)
         {
            Slave.State = SlaveState_ProcessingRequiredAction;
         }
         else
         {
            Slave.State = SlaveState_FormattingErrorReply;
         }
         break;

      case SlaveState_ProcessingRequiredAction:
         ProcessRequest();
         if(true == Slave.IsProcessingActionSuccessful)
         {
            if(IsUnicastMode() == true)
            {
               Slave.State = SlaveState_FormattingNormalReply;
            }
            else // Broadcast mode
            {
               Slave.State = SlaveState_Idle;
            }
         }
         else
         {
#warning "An exception is needed if processing failed"
            Slave.State = SlaveState_FormattingErrorReply;
         }
         break;

      case SlaveState_FormattingNormalReply:
         if(SlaveEvent_NormalReplySent == CurrentEvent)
         {
            Slave.ReplyReady = false;
            Slave.State = SlaveState_Idle;
         }
         else if(false == Slave.ReplyReady)
         {
            Slave.ReplyReady = true;
            BuildNormalReply();

            ModbusRTU_TransmitResponse(&Slave.Response);
         }
         else { /* Do nothing */ }
         break;

      case SlaveState_FormattingErrorReply:
         if(SlaveEvent_ErrorReplySent == CurrentEvent)
         {
            Slave.ReplyReady = false;
            Slave.State = SlaveState_Idle;
         }
         else if(false == Slave.ReplyReady)
         {
            Slave.ReplyReady = true;
            BuildErrorReply();

            ModbusRTU_TransmitError(&Slave.Error);
         }
         else { /* Do nothing */ }
         break;

      default:
         break;
   }
}

static void ProcessRequest(void)
{
   Slave.IsProcessingActionSuccessful = true;

   switch(Slave.Request.FunctionCode)
   {
   case FunctionCode_ReadHoldingRegisters:
      ModbusData_RefreshHoldingRegisters(Slave.Request.StartingAddress, Slave.Request.StartingAddress+Slave.Request.RegistersQuantity);
      break;

   case FunctionCode_WriteMultipleRegisters:
      ModbusData_WriteHoldingRegisters(Slave.Request.StartingAddress,
                                       Slave.Request.RegistersQuantity,
                                       GetFrameRegistersValueAddress());
      break;

   default:
      break;
   }
}

static void BuildNormalReply(void)
{
   uint16_t i;
   Slave.Response.FunctionCode = Slave.Request.FunctionCode;

   switch(Slave.Response.FunctionCode)
   {
   case FunctionCode_ReadHoldingRegisters:
      Slave.Response.ByteCount = 2 * Slave.Request.RegistersQuantity;
      Slave.Response.DataBuffer[0] = Slave.Response.ByteCount;
      for(i = 0; i < Slave.Response.ByteCount; i++)
      {
         /* Add ByteCount size as offset */
         Slave.Response.DataBuffer[ReadHOldingRegisters_ByteCountSize+2*i] = (uint8_t)(ModbusData_GetHoldingRegisters()[Slave.Request.StartingAddress + i] >> 8);
         Slave.Response.DataBuffer[ReadHOldingRegisters_ByteCountSize+2*i+1] = (uint8_t)(ModbusData_GetHoldingRegisters()[Slave.Request.StartingAddress + i] & 0xFF);
      }
      Slave.Response.DataSize = ReadHOldingRegisters_ByteCountSize + Slave.Response.ByteCount;
      break;

   case FunctionCode_WriteMultipleRegisters:
      Slave.Response.DataBuffer[0] = ModbusUtils_GetHighByte(Slave.Request.StartingAddress);
      Slave.Response.DataBuffer[1] = ModbusUtils_GetLowByte(Slave.Request.StartingAddress);
      Slave.Response.DataBuffer[2] = ModbusUtils_GetHighByte(Slave.Request.RegistersQuantity);
      Slave.Response.DataBuffer[3] = ModbusUtils_GetLowByte(Slave.Request.RegistersQuantity);
      Slave.Response.DataSize = WriteMultipleRegisters_DataSize;
      break;

   default:
      break;
   }
}

static void BuildErrorReply(void)
{
   Slave.Error.ErrorCode = Slave.Request.FunctionCode | 0x80;

   switch(Slave.Error.ErrorCode)
   {
   case ErrorCode_ReadHoldingRegisters:
   case ErrorCode_WriteMultipleRegisters:
      Slave.Error.Exception = Slave.Exception;
      break;

   default:
      break;
   }
}

static bool IsRequestCorrect(void)
{
   /* Check Function Code */
   Slave.Request.FunctionCode = (FunctionCode_T)GetFrameFunctionCodeField();
   switch(Slave.Request.FunctionCode)
   {
   case FunctionCode_ReadHoldingRegisters:
      Slave.Request.RegistersQuantity = GetFrameRegistersQuantityField();
      if(   Slave.Request.RegistersQuantity < ReadHoldingRegisters_MinRegQty
         || Slave.Request.RegistersQuantity > ReadHoldingRegisters_MaxRegQty)
      {
         Slave.Exception = 0x03;
         return false;
      }

      Slave.Request.StartingAddress = GetFrameStartingAddressField();
      if(   Slave.Request.StartingAddress >= ModbusData_HoldingRegistersQty
         || (Slave.Request.StartingAddress + Slave.Request.RegistersQuantity) > ModbusData_HoldingRegistersQty)
      {
         Slave.Exception = 0x02;
         return false;
      }
      break;

   case FunctionCode_WriteMultipleRegisters:
      Slave.Request.RegistersQuantity = GetFrameRegistersQuantityField();
      if(   Slave.Request.RegistersQuantity < WriteMultipleRegisters_MinReqQty
         || Slave.Request.RegistersQuantity > WriteMultipleRegisters_MaxReqQty
         || GetFrameByteCountField() != (Slave.Request.RegistersQuantity * 2))
      {
         Slave.Exception = 0x03;
         return false;
      }

      Slave.Request.StartingAddress = GetFrameStartingAddressField();
      if(   Slave.Request.StartingAddress >= ModbusData_HoldingRegistersQty
         || (Slave.Request.StartingAddress + Slave.Request.RegistersQuantity) > ModbusData_HoldingRegistersQty)
      {
         Slave.Exception = 0x02;
         return false;
      }
      break;

   default:
      Slave.Exception = 0x01;
      return false;
      break;
   }

   return true;
}

static bool IsUnicastMode(void)
{
   return ModbusBroadcastAddress != Slave.RxBuffer[0] ? true : false;
}

static uint8_t GetFrameFunctionCodeField(void)
{
   /* Assume that buffer was cleared before new frame reception
    * and therefore count from beginning.
    */
   uint8_t FunctionCode;

   FunctionCode = Slave.RxBuffer[1];
   return FunctionCode;
}

static uint8_t GetFrameByteCountField(void)
{
   /* Assume that buffer was cleared before new frame reception
    * and therefore count from beginning.
    */
   uint8_t ByteCount;

   ByteCount = Slave.RxBuffer[WriteMultipleRegisters_ByteCountField];

   return ByteCount;
}

static uint16_t GetFrameRegistersQuantityField(void)
{
   /* Assume that buffer was cleared before new frame reception
    * and therefore count from beginning.
    */
   uint8_t tmp;
   uint16_t RegistersQuantity = 0u;

   tmp = Slave.RxBuffer[5];
   RegistersQuantity |= (uint16_t)tmp;
   tmp = Slave.RxBuffer[4];
   RegistersQuantity |= (uint16_t)(tmp << 8);

   return RegistersQuantity;
}

static uint16_t GetFrameStartingAddressField(void)
{
   /* Assume that buffer was cleared before new frame reception
    * and therefore count from beginning.
    */
   uint8_t tmp;
   uint16_t StartingAddress = 0u;

   tmp = Slave.RxBuffer[3];
   StartingAddress |= (uint16_t)tmp;
   tmp = Slave.RxBuffer[2];
   StartingAddress |= (uint16_t)(tmp << 8);

   return StartingAddress;
}

static uint8_t* GetFrameRegistersValueAddress(void)
{
   /* Assume that buffer was cleared before new frame reception
    * and therefore count from beginning.
    */

   return &Slave.RxBuffer[7];
}

static inline void SetEvent(ModbusSlaveEvent_T Event)
{
   Slave.Event = Event;
}

void ModbusSlave_ReceivedRequestCallback(void)
{
   SetEvent(SlaveEvent_ReceivedRequest);
}

void ModbusSlave_NormalReplySentCallback(void)
{
   SetEvent(SlaveEvent_NormalReplySent);
}

void ModbusSlave_ErrorReplySentCallback(void)
{
   SetEvent(SlaveEvent_ErrorReplySent);
}
