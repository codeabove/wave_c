/*
 * ModbusData.h
 *
 *  Created on: 21 lut 2018
 *      Author: Lukasz
 */

#ifndef __ModbusData_H_
#define __ModbusData_H_

void ModbusData_Init(void);
void ModbusData_RefreshHoldingRegisters(uint16_t StartAddress, uint16_t EndAddress);
void ModbusData_WriteHoldingRegisters(uint16_t StartAddress, uint16_t RegistersQuantity, uint8_t* Buffer);

uint16_t* ModbusData_GetHoldingRegisters(void);

#endif /* __ModbusData_H_ */
