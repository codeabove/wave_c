/*
 * ModbusUtils.h
 *
 *  Created on: 4 kwi 2018
 *      Author: Lukasz
 */

#ifndef __ModbusUtils_H_
#define __ModbusUtils_H_

#include "stdint.h"

uint8_t ModbusUtils_GetHighByte(uint16_t Value);
void ModbusUtils_SetHighByte(uint16_t *Value, uint8_t Byte);

uint8_t ModbusUtils_GetLowByte(uint16_t Value);
void ModbusUtils_SetLowByte(uint16_t *Value, uint8_t Byte);

#endif /* __ModbusUtils_H_ */
