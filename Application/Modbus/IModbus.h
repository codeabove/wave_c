/*
 * IModbus.h
 *
 *  Created on: 3 mar 2018
 *      Author: Lukasz
 */

#ifndef __IModbus_H_
#define __IModbus_H_

#include "stm32l4xx.h"
#include "MCU/Communication/USART1.h"
#include "MCU/Timers/TIM6.h"
#include "MCU/Timers/TIM7.h"

#define ModbusRTU_Write                         ( USART1_Write                      )
#define ModbusRTU_EnableTransmitInterrupt       ( USART1_EnableTransmitInterrupt    )
#define ModbusRTU_DisableTransmitInterrupt      ( USART1_DisableTransmitInterrupt   )
#define ModbusRTU_RequestTxInterrupt            ( USART1_RequestTxInterrupt         )
#define ModbusRTU_IsTxTransmissionInProgress    ( USART1_IsTxTransmissionInProgress )

#define ModbusRTU_ResetT15Counter               ( TIM6_ResetCounter                 )
#define ModbusRTU_StartT15                      ( TIM6_Start                        )
#define ModbusRTU_ResetT35Counter               ( TIM7_ResetCounter                 )
#define ModbusRTU_StartT35                      ( TIM7_Start                        )

#define ModbusSlave_DisableCommunicationInterrupts() ( NVIC_DisableIRQ(USART1_IRQn) )
#define ModbusSlave_EnableCommunicationInterrupts()  ( NVIC_EnableIRQ(USART1_IRQn)  )
#define ModbusSlave_DisableT35Interrupts()           ( NVIC_DisableIRQ(TIM7_IRQn)   )
#define ModbusSlave_EnableT35Interrupts()            ( NVIC_EnableIRQ(TIM7_IRQn)    )

#endif /* __IModbus_H_ */
