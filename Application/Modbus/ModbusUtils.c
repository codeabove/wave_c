/*
 * ModbusUtils.c
 *
 *  Created on: 4 kwi 2018
 *      Author: Lukasz
 */

#include "ModbusUtils.h"

inline uint8_t ModbusUtils_GetHighByte(uint16_t Value)
{
   return (uint8_t)(Value >> 8);
}

inline void ModbusUtils_SetHighByte(uint16_t *Value, uint8_t Byte)
{
   *Value |= (uint16_t)(Byte << 8);
}

inline uint8_t ModbusUtils_GetLowByte(uint16_t Value)
{
   return (uint8_t)(Value & 0xFF);
}

inline void ModbusUtils_SetLowByte(uint16_t *Value, uint8_t Byte)
{
   *Value |= (uint16_t)Byte;
}

