/*
 * ModbusDataUsr.c
 *
 *  Created on: 17 mar 2018
 *      Author: Lukasz
 */

#include "stdint.h"
#include "stdlib.h"
#include "ModbusDataUsr.h"

#include "Drivers/BatteryMeter/BatteryMeterDriver.h"
#include "Drivers/Encoder/EncoderDriver.h"
#include "Drivers/Proximity/ProximityDriver.h"
#include "Drivers/Proximity/ProximityDriverConfig.h"
#include "Drivers/TB6612FNG/TB6612FNG.h"

static uint16_t ModbusDataUsr_GetProximitySensor1(void);
static uint16_t ModbusDataUsr_GetProximitySensor2(void);
static uint16_t ModbusDataUsr_GetProximitySensor3(void);
static uint16_t ModbusDataUsr_GetProximitySensor4(void);
static uint16_t ModbusDataUsr_GetMotorDutyCycleLeft(void);
static uint16_t ModbusDataUsr_GetMotorDutyCycleRight(void);
static uint16_t ModbusDataUsr_GetEncoderLeftHigh(void);
static uint16_t ModbusDataUsr_GetEncoderLeftLow(void);
static uint16_t ModbusDataUsr_GetEncoderRightHigh(void);
static uint16_t ModbusDataUsr_GetEncoderRightLow(void);

static void ModbusDataUsr_SetMotorDutyCycleLeft(uint16_t DutyCycle);
static void ModbusDataUsr_SetMotorDutyCycleRight(uint16_t DutyCycle);

void ModbusDataUsr_InitHoldingRegisterRefreshFunctions(RefreshFunction *HoldingRegistersRefreshFunctions)
{
   HoldingRegistersRefreshFunctions[0]  = BatteryMeter_GetBatteryVoltage;
   HoldingRegistersRefreshFunctions[1]  = ModbusDataUsr_GetProximitySensor1;
   HoldingRegistersRefreshFunctions[2]  = ModbusDataUsr_GetProximitySensor2;
   HoldingRegistersRefreshFunctions[3]  = ModbusDataUsr_GetProximitySensor3;
   HoldingRegistersRefreshFunctions[4]  = ModbusDataUsr_GetProximitySensor4;
   HoldingRegistersRefreshFunctions[5]  = ModbusDataUsr_GetMotorDutyCycleLeft;
   HoldingRegistersRefreshFunctions[6]  = ModbusDataUsr_GetMotorDutyCycleRight;
   HoldingRegistersRefreshFunctions[7]  = ModbusDataUsr_GetEncoderLeftHigh;
   HoldingRegistersRefreshFunctions[8]  = ModbusDataUsr_GetEncoderLeftLow;
   HoldingRegistersRefreshFunctions[9]  = ModbusDataUsr_GetEncoderRightHigh;
   HoldingRegistersRefreshFunctions[10] = ModbusDataUsr_GetEncoderRightLow;

}

void ModbusDataUsr_InitHoldingRegisterSetFunctions(SetFunction *HoldingRegistersSetFunctions)
{
   HoldingRegistersSetFunctions[0]  = NULL;
   HoldingRegistersSetFunctions[1]  = NULL;
   HoldingRegistersSetFunctions[2]  = NULL;
   HoldingRegistersSetFunctions[3]  = NULL;
   HoldingRegistersSetFunctions[4]  = NULL;
   HoldingRegistersSetFunctions[5]  = ModbusDataUsr_SetMotorDutyCycleLeft;
   HoldingRegistersSetFunctions[6]  = ModbusDataUsr_SetMotorDutyCycleRight;
   HoldingRegistersSetFunctions[7]  = NULL;
   HoldingRegistersSetFunctions[8]  = NULL;
   HoldingRegistersSetFunctions[9]  = NULL;
   HoldingRegistersSetFunctions[10] = NULL;
}

static uint16_t ModbusDataUsr_GetProximitySensor1(void)
{
   return ProximityDriver_GetResult(ProxDrv_Sensor_1);
}

static uint16_t ModbusDataUsr_GetProximitySensor2(void)
{
   return ProximityDriver_GetResult(ProxDrv_Sensor_2);
}

static uint16_t ModbusDataUsr_GetProximitySensor3(void)
{
   return ProximityDriver_GetResult(ProxDrv_Sensor_3);
}

static uint16_t ModbusDataUsr_GetProximitySensor4(void)
{
   return ProximityDriver_GetResult(ProxDrv_Sensor_4);
}

static uint16_t ModbusDataUsr_GetMotorDutyCycleLeft(void)
{
   return (uint16_t)(abs(TB6612FNG_GetDutyCycle(TB6612FNG_Channel_A)));
}

static void ModbusDataUsr_SetMotorDutyCycleLeft(uint16_t DutyCycle)
{
   TB6612FNG_SetChannel(TB6612FNG_Channel_A, (int16_t)DutyCycle);
}

static uint16_t ModbusDataUsr_GetMotorDutyCycleRight(void)
{
   return (uint16_t)(abs(TB6612FNG_GetDutyCycle(TB6612FNG_Channel_B)));
}

static void ModbusDataUsr_SetMotorDutyCycleRight(uint16_t DutyCycle)
{
   TB6612FNG_SetChannel(TB6612FNG_Channel_B, (int16_t)DutyCycle);
}

static uint16_t ModbusDataUsr_GetEncoderLeftHigh(void)
{
   return (uint16_t)(EncoderDriver_GetCurrentSpeed_RPM(Encoder_Left) >> 16);
}

static uint16_t ModbusDataUsr_GetEncoderLeftLow(void)
{
   return (uint16_t)(EncoderDriver_GetCurrentSpeed_RPM(Encoder_Left) & 0x00FF);
}

static uint16_t ModbusDataUsr_GetEncoderRightHigh(void)
{
   return (uint16_t)(EncoderDriver_GetCurrentSpeed_RPM(Encoder_Right) >> 16);
}

static uint16_t ModbusDataUsr_GetEncoderRightLow(void)
{
   return (uint16_t)(EncoderDriver_GetCurrentSpeed_RPM(Encoder_Right) & 0x00FF);
}


