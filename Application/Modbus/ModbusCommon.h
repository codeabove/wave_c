/*
 * ModbusCommon.h
 *
 *  Created on: 23 lut 2018
 *      Author: Lukasz
 */

#ifndef __ModbusCommon_H_
#define __ModbusCommon_H_

#include "stdint.h"

#define ModbusSlaveAddress          ( 1u )
#define ModbusBroadcastAddress      ( 0u )

#define ModbusPDU_MaxSize ( 253u )

typedef enum FunctionCode
{
   FunctionCode_ReadHoldingRegisters = 0x03,
   FunctionCode_WriteMultipleRegisters = 0x10

   /* This enumeration consists of SUPPORTED function codes.
    * Feel free to add more if needed. */
} FunctionCode_T;

typedef enum ErrorCode
{
   ErrorCode_ReadHoldingRegisters = 0x83,
   ErrorCode_WriteMultipleRegisters = 0x90

   /* This enumeration consists of SUPPORTED error codes.
    * Feel free to add more if needed. */
} ErrorCode_T;

typedef struct ModbusRequest
{
   FunctionCode_T FunctionCode;
   uint16_t StartingAddress;
   uint16_t RegistersQuantity;
} ModbusRequest_T;

typedef struct ModbusResponse
{
   FunctionCode_T FunctionCode;
   uint8_t DataBuffer[ModbusPDU_MaxSize];

   /* Helpers */
   uint8_t DataSize;
   uint8_t ByteCount;
} ModbusResponse_T;

typedef struct ModbusError
{
   ErrorCode_T ErrorCode;
   uint8_t Exception;
} ModbusError_T;

#endif /* __ModbusCommon_H_ */
