/*
 * ModbusRTU.c
 *
 *  Created on: 6 lut 2018
 *      Author: Lukasz
 */

#include "stdbool.h"
#include "stdint.h"
#include "IModbus.h"
#include "ModbusRTU.h"
#include "ModbusSlave.h"
#include "CRC.h"

#include "Framework/FIFO/FIFO.h"
#include "Framework/FIFO/FIFOExt.h"

#define ModbusRTU_RxBufferSize ( 256u )
#define ModbusRTU_TxBufferSize ( 256u )
#define ModbusRTU_SizeOfRxBufferElement ( 1u )
#define ModbusRTU_SizeOfTxBufferElement ( 1u )

#define ModbusReservedAddressStart  ( 248u )
#define ModbusReservedAddressEnd    ( 255u )

#if ( ModbusSlaveAddress == ModbusBroadcastAddress )
#  error "Slave address cannot be equal to Modbus Broadcast address!"
#elif ( ( ModbusSlaveAddress >= ModbusReservedAddressStart ) && ( ModbusSlaveAddress <= ModbusReservedAddressEnd ) )
#  error "Reserved Modbus address selected as slave address!"
#endif

typedef enum ModbusEvent
{
   ModbusEvent_T15Expired = 0u,
   ModbusEvent_T35Expired,
   ModbusEvent_ReceivedByte,
   ModbusEvent_EmissionDemand,
   ModbusEvent_EmittedLastCharacter
} ModbusEvent_T;

typedef enum RTUState
{
   RTUState_Init = 0u,
   RTUState_Idle,
   RTUState_Reception,
   RTUState_ControlAndWaiting,
   RTUState_Emission
} RTUState_T;

typedef struct ModbusRTU
{
   RTUState_T State;
   bool Time15Expired;
   bool Time35Expired;
   bool IsFrameOK;

   FIFO_Buffer_T RxBuffer[ModbusRTU_RxBufferSize];
   FIFO_Buffer_T TxBuffer[ModbusRTU_TxBufferSize];
   FIFO_C RxFIFO;
   FIFO_C TxFIFO;

   bool TransmittingResponse;
   bool TransmittingError;
} ModbusRTU_T;

static void ModbusRTU_Process(ModbusEvent_T Event, uint8_t Data);
static void ModbusRTU_RestartTimers(void);
static void ModbusRTU_DiscardReceivedFrame(void);
static void ModbusRTU_T15Restart(void);
static void ModbusRTU_T35Restart(void);
static bool IsSlaveAddressCorrect(void);
static bool IsFrameCrcCorrect(void);
static uint8_t GetFrameAddressField(void);
static uint16_t GetFrameCRCField(void);
static void FillFrameCRCField(void);

static ModbusRTU_T RTU =
{
      .State = RTUState_Init,
      .Time15Expired = false,
      .Time35Expired = false,
      .IsFrameOK = false,
      .TransmittingResponse = false,
      .TransmittingError = false
};

void ModbusRTU_Init(void)
{
   /* Start T3.5 and wait for timeout before moving to Idle.
    * If any character received, ignore it, restart T3.5 and wait.
    */

   RTU.State = RTUState_Init;

   FIFO_Initialize(&RTU.RxFIFO, RTU.RxBuffer, ModbusRTU_RxBufferSize, ModbusRTU_SizeOfRxBufferElement);
   FIFO_Initialize(&RTU.TxFIFO, RTU.TxBuffer, ModbusRTU_TxBufferSize, ModbusRTU_SizeOfTxBufferElement);

   ModbusRTU_T35Restart();
}

void ModbusRTU_TransmitResponse(ModbusResponse_T *Response)
{
   uint8_t tmp;

   FIFOExt_Reset(&RTU.TxFIFO);

   tmp = ModbusSlaveAddress;
   FIFO_Push(&RTU.TxFIFO, &tmp, 1u);
   FIFO_Push(&RTU.TxFIFO, &Response->FunctionCode, 1u);
   FIFO_Push(&RTU.TxFIFO, Response->DataBuffer, Response->DataSize);

   FillFrameCRCField();

   RTU.TransmittingResponse = true;
   ModbusRTU_Process(ModbusEvent_EmissionDemand, 0);
}

void ModbusRTU_TransmitError(ModbusError_T *Error)
{
   uint8_t tmp;

   FIFOExt_Reset(&RTU.TxFIFO);

   tmp = ModbusSlaveAddress;
   FIFO_Push(&RTU.TxFIFO, &tmp, 1u);
   FIFO_Push(&RTU.TxFIFO, &Error->ErrorCode, 1u);
   FIFO_Push(&RTU.TxFIFO, &Error->Exception, 1u);

   FillFrameCRCField();

   RTU.TransmittingError = true;
   ModbusRTU_Process(ModbusEvent_EmissionDemand, 0);
}

FIFO_Buffer_T* ModbusRTU_GetRxBuffer(void)
{
   return RTU.RxBuffer;
}

static void ModbusRTU_Process(ModbusEvent_T Event, uint8_t Data)
{
   switch(RTU.State)
   {
   case RTUState_Init:
      if(ModbusEvent_ReceivedByte == Event)
      {
         ModbusRTU_RestartTimers();
      }
      else if(ModbusEvent_T35Expired == Event)
      {
         RTU.State = RTUState_Idle;
      }
      else { /* Do nothing */ }
      break;

   case RTUState_Idle:
      if(ModbusEvent_ReceivedByte == Event)
      {
         ModbusRTU_RestartTimers();
         FIFO_Push(&RTU.RxFIFO, &Data, 1u);
         RTU.State = RTUState_Reception;
      }
      else if(ModbusEvent_EmissionDemand == Event)
      {
         ModbusRTU_EnableTransmitInterrupt();
         ModbusRTU_RequestTxInterrupt();
         RTU.State = RTUState_Emission;
      }
      else { /* Do nothing */ }
      break;

   case RTUState_Reception:
      if(ModbusEvent_ReceivedByte == Event)
      {
         ModbusRTU_RestartTimers();
         FIFO_Push(&RTU.RxFIFO, &Data, 1u);
      }
      else if(ModbusEvent_T15Expired == Event)
      {
         RTU.State = RTUState_ControlAndWaiting;
      }
      else { /* Do nothing */ }
      break;

   case RTUState_ControlAndWaiting:
      if(ModbusEvent_ReceivedByte == Event)
      {
         RTU.IsFrameOK = false;
      }
      else if(ModbusEvent_T35Expired == Event)
      {
         RTU.IsFrameOK = true;

         /* Control frame
          * 1. Slave address
          * 2. Parity - checked by hardware USART module
          *    TODO: Get Parity error flag
          * 3. CRC */
         if(IsSlaveAddressCorrect() == false)
         {
            RTU.IsFrameOK = false;
         }

         if(IsFrameCrcCorrect() == false)
         {
            RTU.IsFrameOK = false;
         }

         if(true == RTU.IsFrameOK)
         {
            ModbusSlave_ReceivedRequestCallback();
         }
         else //false == RTU.IsFrameOK
         {
            ModbusRTU_DiscardReceivedFrame();
         }

         RTU.State = RTUState_Idle;
      }
      break;

   case RTUState_Emission:
      if(ModbusEvent_EmittedLastCharacter == Event)
      {
         /* Request frame is not needed anymore */
         ModbusRTU_DiscardReceivedFrame();
         ModbusRTU_T35Restart();
      }
      else if(ModbusEvent_T35Expired == Event)
      {
         RTU.State = RTUState_Idle;
      }
      break;

   default:
      break;
   }
}

static void ModbusRTU_RestartTimers(void)
{
   ModbusRTU_T15Restart();
   ModbusRTU_T35Restart();
}

static void ModbusRTU_DiscardReceivedFrame(void)
{
   /* Just reset FIFO */
   FIFOExt_Reset(&RTU.RxFIFO);
}

static void ModbusRTU_T15Restart(void)
{
   RTU.Time15Expired = false;

   ModbusRTU_ResetT15Counter();
   ModbusRTU_StartT15();
}

static void ModbusRTU_T35Restart(void)
{
   RTU.Time35Expired = false;

   ModbusRTU_ResetT35Counter();
   ModbusRTU_StartT35();
}

static bool IsSlaveAddressCorrect(void)
{
   uint8_t Address = GetFrameAddressField();

   if(   Address == ModbusSlaveAddress
      || Address == ModbusBroadcastAddress)
   {
      return true;
   }
   return false;
}

static bool IsFrameCrcCorrect(void)
{
   uint16_t FrameCRC = GetFrameCRCField();
   /* -2 -> Don't take CRC bytes into count when doing this */
   uint16_t CalculatedCRC = CRC16(RTU.RxBuffer, FIFO_AmountDataStored(&RTU.RxFIFO)-2);

   return (FrameCRC == CalculatedCRC ? true : false);
}

static uint8_t GetFrameAddressField(void)
{
   /* Assume that buffer was cleared before new frame reception
    * and therefore count from beginning.
    */
   uint8_t Address;

   Address = RTU.RxBuffer[0];
   return Address;
}

static uint16_t GetFrameCRCField(void)
{
   uint8_t tmp;
   uint16_t FrameCRC = 0u;
   uint16_t FrameSize = FIFO_AmountDataStored(&RTU.RxFIFO);

   tmp = RTU.RxBuffer[FrameSize-1];
   FrameCRC |= (uint16_t)tmp;
   tmp = RTU.RxBuffer[FrameSize-2];
   FrameCRC |= (uint16_t)(tmp << 8);

   return FrameCRC;
}

static void FillFrameCRCField(void)
{
   uint8_t tmp;
   uint16_t CalculatedCRC;

   CalculatedCRC = CRC16(RTU.TxBuffer, FIFO_AmountDataStored(&RTU.TxFIFO));
   tmp = (uint8_t)(CalculatedCRC >> 8);
   FIFO_Push(&RTU.TxFIFO, &tmp, 1u);
   tmp = (uint8_t)(CalculatedCRC & 0xFF);
   FIFO_Push(&RTU.TxFIFO, &tmp, 1u);
}

void ModbusRTU_ReceiveCallback(uint8_t Byte)
{
   ModbusRTU_Process(ModbusEvent_ReceivedByte, Byte);
}

void ModbusRTU_TransmitCallback(void)
{
   /* Handle this case here and pass event to Process only if finished transmission */
   uint8_t tmp = 0u;

   if(!FIFO_IsEmpty(&RTU.TxFIFO))
   {
      FIFO_Pop(&RTU.TxFIFO, &tmp, 1u);
      ModbusRTU_Write(tmp);
   }
   else
   {
      ModbusRTU_DisableTransmitInterrupt();

      if(true == RTU.TransmittingResponse)
      {
         RTU.TransmittingResponse = false;
         ModbusSlave_NormalReplySentCallback();
      }
      else if(true == RTU.TransmittingError)
      {
         RTU.TransmittingError = false;
         ModbusSlave_ErrorReplySentCallback();
      }
      else { /* Do nothing, should NOT reach this place */ }

      ModbusRTU_Process(ModbusEvent_EmittedLastCharacter, 0);
   }
}

void ModbusRTU_TimerT15Callback(void)
{
   RTU.Time15Expired = true;
   ModbusRTU_Process(ModbusEvent_T15Expired, 0);
}

void ModbusRTU_TimerT35Callback(void)
{
   RTU.Time35Expired = true;
   ModbusRTU_Process(ModbusEvent_T35Expired, 0);
}
