/*
 * Application.h
 *
 *  Created on: 04.03.2017
 *      Author: Lukasz
 */

#pragma once

#ifndef __Application_H
#define __Application_H

class Application
{
public:
    Application();
    ~Application();
};

#endif /* __Application_H */
