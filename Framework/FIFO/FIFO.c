/*
 * FIFO.c
 *
 *  Created on: 14.04.2017
 *      Author: Lukasz
 */

#include "stdint.h"
#include "stdbool.h"

#include "FIFO.h"

void FIFO_Initialize(FIFO_C *This, uint8_t *Buffer, uint16_t BufferSize, uint8_t DataSize)
{
   This->Buffer = Buffer;
   This->BufferSize = BufferSize;
   This->DataSize = DataSize;
   This->HeadIndex = 0;
   This->DataCounter = 0;
}

bool FIFO_Push(FIFO_C *This, void *Src, uint16_t DataSize)
{
   uint16_t i;
   uint32_t FIFOIndex;
   uint32_t NumberOfBytes;

   bool DataOverwritten = false;

   NumberOfBytes = DataSize * This->DataSize;

   for(i = 0u; i < NumberOfBytes; i++)
   {
      FIFOIndex = (uint32_t)This->HeadIndex + (uint32_t)This->DataCounter;

      if(FIFOIndex < (uint32_t)This->BufferSize)
      {
         This->Buffer[FIFOIndex] = *((uint8_t *)Src + i);
      }
      else
      {
         This->Buffer[FIFOIndex % (This->BufferSize)] = *((uint8_t *)Src + i);
      }

      if(!FIFO_IsFull(This))
      {
         This->DataCounter++;
      }
      else
      {
         This->HeadIndex++;
         if(This->HeadIndex >= This->BufferSize)
         {
            This->HeadIndex = 0u;
         }

         DataOverwritten = true;
      }
   }

   return DataOverwritten;
}

bool FIFO_Pop(FIFO_C *This, void *Dst, uint16_t DataSize)
{
   uint16_t i, j;
   uint32_t NumberOfBytes;
   bool DataTaken = false;

   NumberOfBytes = DataSize * This->DataSize;

   if(FIFO_IsRequestedAmountDataStored(This, DataSize))
   {
      for(i = 0u; i < NumberOfBytes; )
      {
         for(j = 0u; j < This->DataSize; j++)
         {
            *((uint8_t *)Dst + i + j) = This->Buffer[This->HeadIndex];
            This->DataCounter--;
            This->HeadIndex++;

            if(This->HeadIndex >= This->BufferSize)
            {
               This->HeadIndex = 0u;
            }
         }

         i += This->DataSize;
      }
      DataTaken = true;
   }

   return DataTaken;
}

bool FIFO_IsEmpty(FIFO_C *This)
{
   return (0u == This->DataCounter);
}

bool FIFO_IsFull(FIFO_C *This)
{
   return (This->DataCounter == This->BufferSize);
}

uint16_t FIFO_SpaceAvailable(FIFO_C *This)
{
   return (This->BufferSize - This->DataCounter) / This->DataSize;
}

bool FIFO_IsSpaceAvailable(FIFO_C *This, uint16_t DataSize)
{
   return (FIFO_SpaceAvailable(This) >= DataSize);
}

uint16_t FIFO_AmountDataStored(FIFO_C *This)
{
   return (This->DataCounter / This->DataSize);
}

bool FIFO_IsRequestedAmountDataStored(FIFO_C *This, uint16_t DataSize)
{
   return (FIFO_AmountDataStored(This) >= DataSize);
}
