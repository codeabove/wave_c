/*
 * FIFOExt.h
 *
 *  Created on: 10 lut 2018
 *      Author: Lukasz
 */

#ifndef __FIFOExt_H_
#define __FIFOExt_H_

#include "FIFO.h"

void FIFOExt_GetElement(FIFO_C *This, void* Dst, uint8_t Index);
void FIFOExt_Reset(FIFO_C *This);

#endif /* __FIFOExt_H_ */
