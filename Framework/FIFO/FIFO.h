/*
 * FIFO.h
 *
 *  Created on: 14.04.2017
 *      Author: Lukasz
 */

#ifndef __FIFO_H
#define __FIFO_H

#include "stdint.h"
#include "stdbool.h"

typedef uint8_t FIFO_Buffer_T;

typedef struct FIFO
{
   FIFO_Buffer_T *Buffer;
   uint8_t DataSize;
   uint16_t HeadIndex;
   uint16_t DataCounter;
   uint16_t BufferSize;
} FIFO_C;

void FIFO_Initialize(FIFO_C *This, uint8_t *Buffer, uint16_t BufferSize, uint8_t DataSize);

bool FIFO_Push(FIFO_C *This, void *Src, uint16_t DataSize);
bool FIFO_Pop(FIFO_C *This, void *Dst, uint16_t DataSize);

bool FIFO_IsEmpty(FIFO_C *This);
bool FIFO_IsFull(FIFO_C *This);
uint16_t FIFO_SpaceAvailable(FIFO_C *This);
bool FIFO_IsSpaceAvailable(FIFO_C *This, uint16_t DataSize);
uint16_t FIFO_AmountDataStored(FIFO_C *This);
bool FIFO_IsRequestedAmountDataStored(FIFO_C *This, uint16_t DataSize);

#endif /* __FIFO_H */
