/*
 * FIFOExt.c
 *
 *  Created on: 10 lut 2018
 *      Author: Lukasz
 */

#include "FIFO.h"
#include "FIFOExt.h"

void FIFOExt_GetElement(FIFO_C *This, void* Dst, uint8_t Index)
{
   *((uint8_t *)Dst) = This->Buffer[Index];
}

void FIFOExt_Reset(FIFO_C *This)
{
   This->HeadIndex = 0;
   This->DataCounter = 0;
}
