/*
 * StateMachine.h
 *
 *  Created on: Dec 3, 2016
 *      Author: lukasz
 */

#ifndef STATEMACHINE_H_
#define STATEMACHINE_H_

#include <stdint.h>

#define EVENT_NOEVENT 0x00

typedef void (*StateFuncPtr)(void);
typedef uint8_t (*StateFuncPtrWithEvent)(uint8_t, void*);

typedef struct State
{
	StateFuncPtr OnEnter;
	StateFuncPtrWithEvent Execute;
	StateFuncPtr OnExit;
} State_T;

typedef uint8_t Event_T;

typedef struct StateMachine
{
	uint8_t CurrentState;
	uint8_t NextState;
	Event_T Event;
	State_T **States;
} StateMachine_T;

void StateMachine_Perform(StateMachine_T *this, void *dataPtr);

Event_T StateMachine_GetEvent(StateMachine_T *this);
void StateMachine_SetEvent(StateMachine_T *this, Event_T event);


#endif /* STATEMACHINE_H_ */
