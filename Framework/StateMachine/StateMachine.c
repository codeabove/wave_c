/*
 * StateMachine.c
 *
 *  Created on: Dec 3, 2016
 *      Author: lukasz
 */

#include "StateMachine.h"

void StateMachine_Perform(StateMachine_T *this, void *dataPtr)
{
	Event_T event = StateMachine_GetEvent(this);
	this->NextState = this->States[this->CurrentState]->Execute(event, dataPtr);

	if (this->NextState != this->CurrentState)
	{
		this->States[this->CurrentState]->OnExit();
		this->CurrentState = this->NextState;
		this->States[this->CurrentState]->OnEnter();
	}
}

void StateMachine_SetEvent(StateMachine_T *this, Event_T event)
{
	this->Event = event;
}

Event_T StateMachine_GetEvent(StateMachine_T *this)
{
	Event_T tmp = this->Event;
	this->Event = EVENT_NOEVENT;

	return tmp;
}
