
#ifndef __TypeConverter_H
#define __TypeConverter_H

#include "stdint.h"
#include "stdbool.h"

uint16_t TypeConverter_GetLength(uint8_t* Str);
bool TypeConverter_Itoa(int16_t Value, uint8_t* Str, uint16_t Length, uint8_t Base);
bool TypeConverter_Utoa(uint16_t Value, uint8_t* Str, uint16_t Length, uint8_t Base);

#endif   // !defined( __TypeConverter_H )
