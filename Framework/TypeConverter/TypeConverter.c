#include "TypeConverter.h"

/*!
 * Defines minimum base for conversion format.
 */
#define dTypeConvertion_MinimumBase   ( 2U )

/*!
 * Defines maximum base for conversion format.
 */
#define dTypeConvertion_MaximumBase   ( 16U )

static void TypeConverter_privShiftBuffer(uint8_t *cWstr, uint16_t u16StrLength, uint16_t u16Length );
static bool TypeConverter_privConvert(uint8_t *cWstr, uint16_t u16Value, uint16_t u16Length, uint16_t *pu16StrLength, uint8_t u8Base);

/*!
 * \brief   convert integer to string.
 * \details Ansi C "itoa" based on Kernighan & Ritchie's "Ansi C". Function returns false if buffer size is
 *          insufficient and true if buffer size is ok.
 *
 * @param[in]  sValue    - int16_t value which is meant to be converted to String (char* type).
 * @param[in]  pStr      - pointer to the output buffer, size of the buffer must be large enough to contain
 *                         all data. I.e when number is -30, buffer must contain at least 4 bytes ('-','3', '0', \0).
 * @param[in]  u16Lenght - uint16_t, number of elements in pStr buffer.
 * @param[in]  u8Base    - data output format. defines type of conversion, i.e "2" means that sValue will be
 *                         output in binary format, "10" in decimal and "16" in hexadecimal format.
 *                         Valid values for "uBase" are in range from 2 to 35.
 * @param[out] none
 * @return  bool       - returns "false" if buffer size is insufficient and conversion has failed
 */
bool TypeConverter_Itoa( int16_t s16Value, uint8_t* pStr, uint16_t u16Length, uint8_t u8Base )
{
   uint8_t* cWstr = pStr;

   //min length 1 because of '\0' character
   uint16_t u16StrLength = 1;

   int16_t s16Sign;
   // Take care of sign
   if ( 0 > ( s16Sign = s16Value ) )
   {
      s16Value = -s16Value;
   }

   if( !TypeConverter_privConvert( cWstr, s16Value, u16Length, &u16StrLength, u8Base ) )
   {
      return false;
   }

   if ( s16Sign < 0 )
   {
      if( ++u16StrLength > u16Length )
      {
         *cWstr = '\0';
         return false;
      }
      *( cWstr + u16Length - u16StrLength ) = '-';
   }

   TypeConverter_privShiftBuffer( cWstr, u16StrLength, u16Length );

   return true;
}

/*!
 * \brief   convert unsigned integer to string.
 * \details Function returns false if buffer size is insufficient and true if buffer size is ok.
 *
 * @param[in]  u16Value  - uint16_t value which is meant to be converted to String (char* type).
 * @param[in]  pStr      - pointer to the output buffer, size of the buffer must be large enough to contain
 *                         all data. I.e when number is -30, buffer must contain at least 4 bytes ('-','3', '0', \0).
 * @param[in]  u16Lenght - uint16_t, number of elements in pStr buffer.
 * @param[in]  u8Base    - data output format. defines type of conversion, i.e "2" means that sValue will be
 *                         output in binary format, "10" in decimal and "16" in hexadecimal format.
 *                         Valid values for "uBase" are in range from 2 to 35.
 * @param[out] none
 * @return  bool     -   returns "false" if buffer size is insufficient and conversion has failed
 */
bool TypeConverter_Utoa( uint16_t u16Value, uint8_t* pStr, uint16_t u16Length, uint8_t u8Base )
{
   uint8_t* cWstr = pStr;

   //min length 1 because of '\0' character
   uint16_t u16StrLength = 1;

   if( !TypeConverter_privConvert( cWstr, u16Value, u16Length, &u16StrLength, u8Base ) )
   {
      return false;
   }

   TypeConverter_privShiftBuffer( cWstr, u16StrLength, u16Length );

   return true;
}

/*!
 * \brief   Gets length of string.
 * \details Function returns length of string in buffer
 *
 * @param[in]  pStr    -   pointer to the output buffer
 * @param[out] none
 * @return  uin16_t    -   length of string in buffer
 */
uint16_t TypeConverter_GetLength( uint8_t* pStr )
{
   uint8_t* cWstr = pStr;

   uint16_t u16StrLength = 0;

   while ( *cWstr++ != '\0' )
   {
      u16StrLength++;
   }

   return u16StrLength;
}

/*!
 * \brief   convert unsigned integer to array. Output string is right aligned in the buffer
 *          ( i.e. number "123" in 6-byte buffer will look like this: {'0', '0', '0', '1', '2', '/0'} )
 * \details Function returns false if buffer size is insufficient and true if buffer size is ok.
 *
 * @param[in]  cWstr       - pointer to the output buffer, size of the buffer must be large enough to contain
 *                           all data. I.e when number is -30, buffer must contain at least 4 bytes ('-','3', '0', \0).
 * @param[in]  uValue      - uint16_t value which is meant to be converted to String (char* type).
 * @param[in]  uLenght     - uint16_t, number of elements in cWstr buffer.
 * @param[in]  uStrLength  - pointer to variable that contains information about length of the string
 * @param[in]  uBase       - data output format. defines type of conversion, i.e "2" means that sValue will be
 *                           output in binary format, "10" in decimal and "16" in hexadecimal format.
 *                           Valid values for "uBase" are in range from 2 to 35.
 * @param[out] none
 * @return  bool         - returns "false" if buffer size is insufficient and conversion has failed
 */
static bool TypeConverter_privConvert(uint8_t *cWstr, uint16_t u16Value, uint16_t u16Length, uint16_t *pu16StrLength, uint8_t u8Base)
{
   static const uint8_t tNum[ ] = "0123456789abcdefghijklmnopqrstuvwxyz";
   //set pointer at the end of the buffer
   cWstr += u16Length - 1u;

   *cWstr-- = '\0';
   do
   {
      if( ++( *pu16StrLength ) > u16Length )
      {
         *++cWstr = '\0';
         return false;
      }
      *cWstr-- = tNum[ u16Value % u8Base ];
   }
   while ( u16Value /= u8Base );

   return true;
}

/*!
 * \brief   Shifts elements in buffer. Changes alignment from right to left ( i.e.
 *          { '0', '0', '0', '1', '2', '/0' } -> { '1', '2', '/0', '0', '0', '0' }
 * \details Function returns false if buffer size is insufficient and true if buffer size is ok.
 *
 * @param[in]  cWstr       - pointer to the output buffer
 * @param[in]  uStrLength  - uint16_t, variable that contains information about length of the string.
 * @param[in]  uLenght     - uint16_t, number of elements in cWstr buffer.
 * @param[out] none
 * @return     void
 */
static void TypeConverter_privShiftBuffer(uint8_t *cWstr, uint16_t u16StrLength, uint16_t u16Length )
{
   uint16_t u16Diff = u16Length - u16StrLength;

   uint16_t u16Counter = 0;
   //shifting characters in buffer, i.e.: [0 0 0 1 2 3 \0] -> [1 2 3 \0 0 0 0 ]
   for ( ; u16Counter < u16StrLength ; u16Counter++ )
   {
      *cWstr = *( cWstr + u16Diff );
      ++cWstr;
   }

   //filling rest of the buffer with zeros
   for ( ; u16Counter < u16Length ; u16Counter++ )
   {
      *cWstr = 0;
      ++cWstr;
   }
}
