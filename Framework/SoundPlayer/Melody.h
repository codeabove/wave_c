/*
 * Melody.h
 *
 *  Created on: 2 wrz 2017
 *      Author: Lukasz
 */

#ifndef __Melody_H_
#define __Melody_H_

#include "Note.h"

typedef struct MelodyStruct
{
   const uint8_t NotesAmount;
   const Note_T *Notes;
} Melody_T;

typedef enum MelodyEnum
{
   Melody_LowBeep = 0u,
   Melody_MidBeep,
   Melody_HighBeep,
   Melody_Ascending,
   Melody_Descending,
   Melody_ImperialMarch,
   /**************/
   Melody_NumOf
} Melody_E;

extern const Melody_T *Melodies[Melody_NumOf];

#endif /* __Melody_H_ */
