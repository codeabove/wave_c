/*
 * Note.h
 *
 *  Created on: 2 wrz 2017
 *      Author: Lukasz
 */

#ifndef __Note_H_
#define __Note_H_

#include "stdint.h"

#define NOTE_NULL (    0u )
#define NOTE_B0   (   31u )
#define NOTE_C1   (   33u )
#define NOTE_CS1  (   35u )
#define NOTE_D1   (   37u )
#define NOTE_DS1  (   39u )
#define NOTE_E1   (   41u )
#define NOTE_F1   (   44u )
#define NOTE_FS1  (   46u )
#define NOTE_G1   (   49u )
#define NOTE_GS1  (   52u )
#define NOTE_A1   (   55u )
#define NOTE_AS1  (   58u )
#define NOTE_B1   (   62u )
#define NOTE_C2   (   65u )
#define NOTE_CS2  (   69u )
#define NOTE_D2   (   73u )
#define NOTE_DS2  (   78u )
#define NOTE_E2   (   82u )
#define NOTE_F2   (   87u )
#define NOTE_FS2  (   93u )
#define NOTE_G2   (   98u )
#define NOTE_GS2  (  104u )
#define NOTE_A2   (  110u )
#define NOTE_AS2  (  117u )
#define NOTE_B2   (  123u )
#define NOTE_C3   (  131u )
#define NOTE_CS3  (  139u )
#define NOTE_D3   (  147u )
#define NOTE_DS3  (  156u )
#define NOTE_E3   (  165u )
#define NOTE_F3   (  175u )
#define NOTE_FS3  (  185u )
#define NOTE_G3   (  196u )
#define NOTE_GS3  (  208u )
#define NOTE_A3   (  220u )
#define NOTE_AS3  (  233u )
#define NOTE_B3   (  247u )
#define NOTE_C4   (  262u )
#define NOTE_CS4  (  277u )
#define NOTE_D4   (  294u )
#define NOTE_DS4  (  311u )
#define NOTE_E4   (  330u )
#define NOTE_F4   (  349u )
#define NOTE_FS4  (  370u )
#define NOTE_G4   (  392u )
#define NOTE_GS4  (  415u )
#define NOTE_A4   (  440u )
#define NOTE_AS4  (  466u )
#define NOTE_B4   (  494u )
#define NOTE_C5   (  523u )
#define NOTE_CS5  (  554u )
#define NOTE_D5   (  587u )
#define NOTE_DS5  (  622u )
#define NOTE_E5   (  659u )
#define NOTE_F5   (  698u )
#define NOTE_FS5  (  740u )
#define NOTE_G5   (  784u )
#define NOTE_GS5  (  831u )
#define NOTE_A5   (  880u )
#define NOTE_AS5  (  932u )
#define NOTE_B5   (  988u )
#define NOTE_C6   ( 1047u )
#define NOTE_CS6  ( 1109u )
#define NOTE_D6   ( 1175u )
#define NOTE_DS6  ( 1245u )
#define NOTE_E6   ( 1319u )
#define NOTE_F6   ( 1397u )
#define NOTE_FS6  ( 1480u )
#define NOTE_G6   ( 1568u )
#define NOTE_GS6  ( 1661u )
#define NOTE_A6   ( 1760u )
#define NOTE_AS6  ( 1865u )
#define NOTE_B6   ( 1976u )
#define NOTE_C7   ( 2093u )
#define NOTE_CS7  ( 2217u )
#define NOTE_D7   ( 2349u )
#define NOTE_DS7  ( 2489u )
#define NOTE_E7   ( 2637u )
#define NOTE_F7   ( 2794u )
#define NOTE_FS7  ( 2960u )
#define NOTE_G7   ( 3136u )
#define NOTE_GS7  ( 3322u )
#define NOTE_A7   ( 3520u )
#define NOTE_AS7  ( 3729u )
#define NOTE_B7   ( 3951u )
#define NOTE_C8   ( 4186u )
#define NOTE_CS8  ( 4435u )
#define NOTE_D8   ( 4699u )
#define NOTE_DS8  ( 4978u )

typedef struct Note
{
   const uint16_t Frequency;
   const uint16_t Duration;
} Note_T;

#endif /* __Note_H_ */
