/*
 * SoundPlayer.c
 *
 *  Created on: 1 wrz 2017
 *      Author: Lukasz
 */

#include "stdbool.h"
#include "stdint.h"
#include "stm32l4xx.h"
#include "SoundPlayer.h"

#include "MCU/Timers/TIM16.h"

#include "Note.h"
#include "Melody.h"

#define PerformIntervalsTimeMs ( 100u )

#define ClockCyclesPerSecond     ( SystemCoreClock )
#define MaxReload                ( 0xFFFFu   )

typedef struct SoundPlayer
{
   uint32_t TimeCounter;

   bool IsPlayingMelody;
   bool MelodyRequested;

   Melody_E CurrentMelody;
   uint8_t CurrentNoteIndex;
} SoundPlayer_T;
static SoundPlayer_T SoundPlayer;

static void SoundPlayer_PlayFrequency(uint16_t Frequency);

/*!
 * \brief      Function keeps track of played melody and
 *             changes notes when needed
 *
 * @param[in]  void
 * @param[out] void
 */
void SoundPlayer_Perform(void)
{
   if(SoundPlayer.IsPlayingMelody == true)
   {
      SoundPlayer.TimeCounter += PerformIntervalsTimeMs;

      if(SoundPlayer.TimeCounter >= Melodies[SoundPlayer.CurrentMelody]->Notes[SoundPlayer.CurrentNoteIndex].Duration)
      {
         SoundPlayer.CurrentNoteIndex++;
         if(SoundPlayer.CurrentNoteIndex < Melodies[SoundPlayer.CurrentMelody]->NotesAmount)
         {
            SoundPlayer.TimeCounter = 0u;
            SoundPlayer_PlayFrequency(Melodies[SoundPlayer.CurrentMelody]->Notes[SoundPlayer.CurrentNoteIndex].Frequency);
         }
         else
         {
            SoundPlayer.IsPlayingMelody = false;
            SoundPlayer_StopPlaying();
         }
      }
   }
   else if(SoundPlayer.MelodyRequested == true)
   {
      SoundPlayer.MelodyRequested = false;
      SoundPlayer.IsPlayingMelody = true;

      SoundPlayer_PlayFrequency(Melodies[SoundPlayer.CurrentMelody]->Notes[SoundPlayer.CurrentNoteIndex].Frequency);
   }
   else
   {

   }
}

/*!
 * \brief      Function sets up sound player to play
 *             selected melody
 *
 * @param[in]  void
 * @param[out] void
 */
void SoundPlayer_PlayMelody(uint8_t Melody)
{
   if(SoundPlayer.IsPlayingMelody == true)
   {
      SoundPlayer_StopPlaying();
   }

   SoundPlayer.MelodyRequested = true;

   SoundPlayer.CurrentMelody = (Melody_E)Melody;
   SoundPlayer.CurrentNoteIndex = 0u;

   SoundPlayer.TimeCounter = 0u;
}

/*!
 * \brief      Function stops melody playback
 *
 * @param[in]  void
 * @param[out] void
 */
void SoundPlayer_StopPlaying(void)
{
   SoundPlayer.IsPlayingMelody = false;

   TIM16_SetAutoReloadRegister(0u);
   TIM16_SetOutputCompare(0u);
   TIM16_SetPrescaler(0u);
}

/*!
 * \brief      Function calculates and applies timer parameters
 *             based on given frequency and applies
 *
 * @param[in]  uint16_t Frequency
 * @param[out] void
 */
static void SoundPlayer_PlayFrequency(uint16_t Frequency)
{
   uint32_t period_cycles = ClockCyclesPerSecond / Frequency;
   uint16_t prescaler = (uint16_t)(period_cycles / MaxReload + 1);
   uint16_t overflow = (uint16_t)((period_cycles + (prescaler / 2)) / prescaler);
   uint16_t duty = (uint16_t)(overflow / 2);

   TIM16_SetAutoReloadRegister(overflow);
   TIM16_SetOutputCompare(duty);
   TIM16_SetPrescaler(prescaler);
}
