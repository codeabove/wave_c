/*
 * Melody.c
 *
 *  Created on: 2 wrz 2017
 *      Author: Lukasz
 */

#include "stdint.h"
#include "Melody.h"
#include "Note.h"

#define BeepNotesCount              ( 1u )
#define AscendingNotesCount         ( 4u )
#define DescendingNotesCount        ( 4u )
#define ImperialMarchNotesCount     ( 17u )

const Note_T LowBeepNotes[BeepNotesCount] =
{
      [0] = { .Frequency = NOTE_A3, .Duration = 100u }
};

const Melody_T LowBeepMelody =
{
      .NotesAmount = BeepNotesCount,
      .Notes = LowBeepNotes
};

const Note_T MidBeepNotes[BeepNotesCount] =
{
      [0] = { .Frequency = NOTE_A5, .Duration = 100u }
};

const Melody_T MidBeepMelody =
{
      .NotesAmount = BeepNotesCount,
      .Notes = MidBeepNotes
};

const Note_T HighBeepNotes[BeepNotesCount] =
{
      [0] = { .Frequency = NOTE_A6, .Duration = 100u }
};

const Melody_T HighBeepMelody =
{
      .NotesAmount = BeepNotesCount,
      .Notes = HighBeepNotes
};

const Note_T AscendingNotes[AscendingNotesCount] =
{
      [0] = { .Frequency = NOTE_C6, .Duration = 200u },
      [1] = { .Frequency = NOTE_E6, .Duration = 200u },
      [2] = { .Frequency = NOTE_G6, .Duration = 200u },
      [3] = { .Frequency = NOTE_C7, .Duration = 200u },
};

const Melody_T AscendingMelody =
{
      .NotesAmount = AscendingNotesCount,
      .Notes = AscendingNotes
};

const Note_T DescendingNotes[DescendingNotesCount] =
{
      [0] = { .Frequency = NOTE_C7, .Duration = 200u },
      [1] = { .Frequency = NOTE_G6, .Duration = 200u },
      [2] = { .Frequency = NOTE_E6, .Duration = 200u },
      [3] = { .Frequency = NOTE_C6, .Duration = 200u },
};

const Melody_T DescendingMelody =
{
      .NotesAmount = DescendingNotesCount,
      .Notes = DescendingNotes
};

const Note_T ImperialMarchNotes[ImperialMarchNotesCount] =
{
      [0]  = { .Frequency = NOTE_A4,   .Duration = 400u },
      [1]  = { .Frequency = NOTE_NULL, .Duration = 100u },
      [2]  = { .Frequency = NOTE_A4,   .Duration = 400u },
      [3]  = { .Frequency = NOTE_NULL, .Duration = 100u },
      [4]  = { .Frequency = NOTE_A4,   .Duration = 400u },
      [5]  = { .Frequency = NOTE_NULL, .Duration = 100u },
      [6]  = { .Frequency = NOTE_F4,   .Duration = 300u },
      [7]  = { .Frequency = NOTE_NULL, .Duration = 100u },
      [8]  = { .Frequency = NOTE_C5,   .Duration = 100u },
      [9]  = { .Frequency = NOTE_NULL, .Duration = 100u },
      [10] = { .Frequency = NOTE_A4,   .Duration = 400u },
      [11] = { .Frequency = NOTE_NULL, .Duration = 100u },
      [12] = { .Frequency = NOTE_F4,   .Duration = 300u },
      [13] = { .Frequency = NOTE_NULL, .Duration = 100u },
      [14] = { .Frequency = NOTE_C5,   .Duration = 100u },
      [15] = { .Frequency = NOTE_NULL, .Duration = 100u },
      [16] = { .Frequency = NOTE_A4,   .Duration = 800u },
};

const Melody_T ImperialMarchMelody =
{
      .NotesAmount = ImperialMarchNotesCount,
      .Notes = ImperialMarchNotes
};

const Melody_T *Melodies[Melody_NumOf] =
{
      [Melody_LowBeep       ] = &LowBeepMelody,
      [Melody_MidBeep       ] = &MidBeepMelody,
      [Melody_HighBeep      ] = &HighBeepMelody,
      [Melody_Ascending     ] = &AscendingMelody,
      [Melody_Descending    ] = &DescendingMelody,
      [Melody_ImperialMarch ] = &ImperialMarchMelody
};
