/*
 * SoundPlayer.h
 *
 *  Created on: 1 wrz 2017
 *      Author: Lukasz
 */

#ifndef __SoundPlayer_H_
#define __SoundPlayer_H_

void SoundPlayer_Perform(void);

void SoundPlayer_PlayMelody(uint8_t Melody);
void SoundPlayer_StopPlaying(void);

#endif /* __SoundPlayer_H_ */
