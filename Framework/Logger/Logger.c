/*
 * Logger.c
 *
 *  Created on: 04.04.2017
 *      Author: Lukasz
 */

#include "Framework/FIFO/FIFO.h"
#include "Framework/TypeConverter/TypeConverter.h"
#include "Logger.h"

#define Logger_Timestamp_MilisecondsMaximumValue ( 1000u )
#define Logger_Timestamp_SecondsMaximumValue ( 60u )
#define Logger_Timestamp_MinutesMaximumValue ( 60u )
#define Logger_Timestamp_HoursMaximumValue ( 100u )

#define Logger_Timestamp_MilisecondsDigits ( 3u )
#define Logger_Timestamp_SecondsDigits ( 2u )
#define Logger_Timestamp_MinutesDigits ( 2u )
#define Logger_Timestamp_HoursDigits ( 2u )

#define Logger_DecimalBase ( 10u )

#define Logger_ConvertedNumberBufferSize  ( 16u )

typedef struct Logger_Timestamp
{
   volatile uint8_t Hours;
   volatile uint8_t Minutes;
   volatile uint8_t Seconds;
   volatile uint16_t Miliseconds;
} Logger_Timestamp_T;

typedef struct Logger
{
   FIFO_Buffer_T TxBuffer[Logger_TxBufferSize];
   FIFO_C TxFIFO;

   Logger_Timestamp_T Timestamp;
} Logger_C;

Logger_C Logger;

uint16_t TypeConvert_GetLength( uint8_t* pStr );
bool Logger_privWaitForBuffer(uint16_t DataLength);
void Logger_privSendTimestamp(void);

void Logger_Initialize(void)
{
   FIFO_Initialize(&Logger.TxFIFO, Logger.TxBuffer, Logger_TxBufferSize, Logger_SizeOfTxBufferElement);
   Logger_TimestampReset();
}

void Logger_TransmitCallback(void)
{
   uint8_t Data = 0u;

   if(!FIFO_IsEmpty(&Logger.TxFIFO))
   {
      FIFO_Pop(&Logger.TxFIFO, &Data, 1u);
      Logger_Write(Data);
   }
   else
   {
      Logger_DisableTransmitInterrupt();
   }
}

void Logger_TimestampPerform(void)
{
   Logger.Timestamp.Miliseconds += Logger_PeriodicTaskInterval;

   if ( Logger_Timestamp_MilisecondsMaximumValue == Logger.Timestamp.Miliseconds )
   {
      Logger.Timestamp.Seconds++;
      Logger.Timestamp.Miliseconds = 0U;
   }

   if ( Logger_Timestamp_SecondsMaximumValue == Logger.Timestamp.Seconds )
   {
      Logger.Timestamp.Minutes++;
      Logger.Timestamp.Seconds = 0U;
   }

   if ( Logger_Timestamp_MinutesMaximumValue == Logger.Timestamp.Minutes )
   {
      Logger.Timestamp.Hours++;
      Logger.Timestamp.Minutes = 0U;
   }

   if ( Logger_Timestamp_HoursMaximumValue == Logger.Timestamp.Hours )
   {
      Logger.Timestamp.Hours = 0U;
   }
}

void Logger_SendBinary(uint8_t Data)
{
   Logger_privWaitForBuffer(Logger_OneByte);
   Logger_DisableTransmitInterrupt();
   FIFO_Push(&Logger.TxFIFO, &Data, Logger_OneByte);
   Logger_EnableTransmitInterrupt();

   if(!Logger_IsTxTransmissionInProgress())
   {
      Logger_RequestTxInterrupt();
   }
}

void Logger_SendText(uint8_t *Data, bool TimestampOn)
{
   uint16_t DataLength = 0u;

   if ( TimestampOn )
   {
      Logger_privSendTimestamp();
   }

   DataLength = TypeConvert_GetLength(Data);
   Logger_privWaitForBuffer(DataLength);
   Logger_DisableTransmitInterrupt();
   FIFO_Push(&Logger.TxFIFO, Data, DataLength);
   Logger_EnableTransmitInterrupt();

   if(!Logger_IsTxTransmissionInProgress())
   {
      Logger_RequestTxInterrupt();
   }
}

void Logger_SendUnsignedNumber(uint32_t Number, uint8_t Base, bool TimestampOn)
{
   uint16_t DataLength = 0u;

   /* Buffers for string representation of a number - one is additional for '\0' */
   uint8_t szNumber[ Logger_ConvertedNumberBufferSize + 1U ];

   if ( TimestampOn )
   {
      Logger_privSendTimestamp();
   }

   TypeConverter_Utoa( Number, szNumber, sizeof( szNumber ), Base );

   DataLength = TypeConvert_GetLength( szNumber );
   Logger_privWaitForBuffer( DataLength );
   Logger_DisableTransmitInterrupt();
   FIFO_Push( &Logger.TxFIFO, szNumber, DataLength );
   Logger_EnableTransmitInterrupt();

   if(!Logger_IsTxTransmissionInProgress())
   {
      Logger_RequestTxInterrupt();
   }
}

void Logger_SendSignedNumber(int32_t Number, uint8_t Base, bool TimestampOn)
{
   uint16_t DataLength = 0u;

   /* Buffers for string representation of a number - one is additional for '\0' */
   uint8_t szNumber[ Logger_ConvertedNumberBufferSize + 1U ];

   if ( TimestampOn )
   {
      Logger_privSendTimestamp();
   }

   TypeConverter_Itoa( Number, szNumber, sizeof( szNumber ), Base );

   DataLength = TypeConvert_GetLength( szNumber );
   Logger_privWaitForBuffer( DataLength );
   Logger_DisableTransmitInterrupt();
   FIFO_Push( &Logger.TxFIFO, szNumber, DataLength );
   Logger_EnableTransmitInterrupt();

   if(!Logger_IsTxTransmissionInProgress())
   {
      Logger_RequestTxInterrupt();
   }
}

void Logger_TimestampReset(void)
{
   Logger.Timestamp.Hours = 0u;
   Logger.Timestamp.Minutes = 0u;
   Logger.Timestamp.Seconds = 0u;
   Logger.Timestamp.Miliseconds = 0u;
}

uint16_t TypeConvert_GetLength( uint8_t* pStr )
{
   uint8_t* cWstr = pStr;

   uint16_t u16StrLength = 0;

   while ( *cWstr++ != '\0' )
   {
      u16StrLength++;
   }

   return u16StrLength;
}

bool Logger_privWaitForBuffer(uint16_t DataLength)
{
   bool TimeoutOccured;

   uint32_t TimeoutCounter = 0u;

   while(!FIFO_IsSpaceAvailable(&Logger.TxFIFO, DataLength))
   {
      TimeoutCounter++;
      if(!Logger_IsTxTransmissionInProgress())
      {
         Logger_RequestTxInterrupt();
      }
      if(TimeoutCounter > Logger_QueueTimeout)
      {
         Logger_DisableTransmitInterrupt();
         FIFO_Push(&Logger.TxFIFO, "Tx FIFO overwrite occurred!\r", 28u);
         Logger_EnableTransmitInterrupt();
      }
      TimeoutOccured = true;
   }

   return TimeoutOccured;
}

void Logger_privSendTimestamp(void)
{
   uint8_t i;

   uint8_t HoursStr[Logger_Timestamp_HoursDigits + 1u];
   uint8_t MinutesStr[Logger_Timestamp_MinutesDigits + 1u];
   uint8_t SecondsStr[Logger_Timestamp_SecondsDigits + 1u];
   uint8_t MilisecondsStr[Logger_Timestamp_MilisecondsDigits + 1u];

   uint16_t DataLength = 0u;

   if(TypeConverter_Utoa((uint16_t)Logger.Timestamp.Hours, HoursStr, sizeof(HoursStr), Logger_DecimalBase))
   {
      DataLength = TypeConvert_GetLength(HoursStr);
      Logger_privWaitForBuffer(Logger_Timestamp_HoursDigits + 1u);
      Logger_DisableTransmitInterrupt();
      if(Logger_Timestamp_HoursDigits > DataLength)
      {
         for(i = 0; i < (Logger_Timestamp_HoursDigits - DataLength); i++)
         {
            FIFO_Push(&Logger.TxFIFO, "0", Logger_OneByte);
         }
      }

      FIFO_Push(&Logger.TxFIFO, HoursStr, DataLength);
   }

   FIFO_Push(&Logger.TxFIFO, ":", Logger_OneByte);
   Logger_EnableTransmitInterrupt();

   if(TypeConverter_Utoa((uint16_t)Logger.Timestamp.Minutes, MinutesStr, sizeof(MinutesStr), Logger_DecimalBase))
   {
      DataLength = TypeConvert_GetLength(MinutesStr);
      Logger_privWaitForBuffer(Logger_Timestamp_MinutesDigits + 1u);
      Logger_DisableTransmitInterrupt();
      if(Logger_Timestamp_MinutesDigits > DataLength)
      {
         for(i = 0; i < (Logger_Timestamp_MinutesDigits - DataLength); i++)
         {
            FIFO_Push(&Logger.TxFIFO, "0", Logger_OneByte);
         }
      }

      FIFO_Push(&Logger.TxFIFO, MinutesStr, DataLength);
   }

   FIFO_Push(&Logger.TxFIFO, ":", Logger_OneByte);
   Logger_EnableTransmitInterrupt();

   if(TypeConverter_Utoa((uint16_t)Logger.Timestamp.Seconds, SecondsStr, sizeof(SecondsStr), Logger_DecimalBase))
   {
      DataLength = TypeConvert_GetLength(SecondsStr);
      Logger_privWaitForBuffer(Logger_Timestamp_SecondsDigits + 1u);
      Logger_DisableTransmitInterrupt();
      if(Logger_Timestamp_SecondsDigits > DataLength)
      {
         for(i = 0; i < (Logger_Timestamp_SecondsDigits - DataLength); i++)
         {
            FIFO_Push(&Logger.TxFIFO, "0", Logger_OneByte);
         }
      }

      FIFO_Push(&Logger.TxFIFO, SecondsStr, DataLength);
   }

   FIFO_Push(&Logger.TxFIFO, ".", Logger_OneByte);
   Logger_EnableTransmitInterrupt();

   if(TypeConverter_Utoa((uint16_t)Logger.Timestamp.Miliseconds, MilisecondsStr, sizeof(MilisecondsStr), Logger_DecimalBase))
   {
      DataLength = TypeConvert_GetLength(MilisecondsStr);
      Logger_privWaitForBuffer(Logger_Timestamp_MilisecondsDigits + 1u);
      Logger_DisableTransmitInterrupt();
      if(Logger_Timestamp_MilisecondsDigits > DataLength)
      {
         for(i = 0; i < (Logger_Timestamp_MilisecondsDigits - DataLength); i++)
         {
            FIFO_Push(&Logger.TxFIFO, "0", Logger_OneByte);
         }
      }

      FIFO_Push(&Logger.TxFIFO, MilisecondsStr, DataLength);
   }

   FIFO_Push(&Logger.TxFIFO, " ", Logger_OneByte);
   Logger_EnableTransmitInterrupt();
}

