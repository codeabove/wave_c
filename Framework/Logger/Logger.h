/*
 * Logger.h
 *
 *  Created on: 04.04.2017
 *      Author: Lukasz
 */

#ifndef __Logger_H
#define __Logger_H

#include "MCU/Communication/USART3.h"

#define Logger_OneByte ( 1u )

#define Logger_TxBufferSize ( 512u )
#define Logger_SizeOfTxBufferElement ( Logger_OneByte * 1u )

#define Logger_QueueTimeout ( 3000u )

#define Logger_PeriodicTaskInterval ( 1u )

#define Logger_Write                         USART3_Write
#define Logger_EnableTransmitInterrupt       USART3_EnableTransmitInterrupt
#define Logger_DisableTransmitInterrupt      USART3_DisableTransmitInterrupt
#define Logger_RequestTxInterrupt            USART3_RequestTxInterrupt
#define Logger_IsTxTransmissionInProgress    USART3_IsTxTransmissionInProgress

void Logger_Initialize(void);
void Logger_TimestampPerform(void);

void Logger_TransmitCallback(void);

void Logger_SendBinary(uint8_t Data);
void Logger_SendText(uint8_t *Data, bool TimestampOn);
void Logger_SendUnsignedNumber(uint32_t Number, uint8_t Base, bool TimestampOn);
void Logger_SendSignedNumber(int32_t Number, uint8_t Base, bool TimestampOn);

void Logger_TimestampReset(void);

#endif /* __Logger_H */
