/*
 * PID.c
 *
 *  Created on: 25 wrz 2017
 *      Author: Lukasz
 */

#include "stdbool.h"
#include "float.h"
#include "PID.h"

void PID_Initialize(PID_T *this)
{
   this->IterationTime = 1.0f;
   this->Output = 0.0f;
   this->SaturationOutput = 0.0f;
   this->OutputSaturated = false;

   this->OutputMax = FLT_MAX;
   this->OutputMin = FLT_MIN;

   this->Integral = 0.0f;
   this->IntegralLock = false;
}

/*!
 * \brief
 * \details
 *
 * @param[in]  float Setpoint - regulation target value
 * @param[in]  float Actual - regulation current value
 * @param[out] float - regulation processing output
 */
float PID_Process(PID_T *this, float Setpoint, float Actual)
{
   /* Error */
   this->Error = Setpoint - Actual;

   /* P */
   this->Proportional = this->GainP * this->Error;

   /* I */
   if(this->IntegralLock == false)
   {
      this->Integral += (this->GainI * this->Error * this->IterationTime);
   }

   /* D */
   this->Derivative = this->GainD * (this->Error - this->LastError) / this->IterationTime;

   /* Output */
   this->Output = this->Proportional + this->Integral + this->Derivative;

   /* Check output saturation */
   if(this->Output > this->OutputMax)
   {
      this->SaturationOutput = this->OutputMax;
      this->OutputSaturated = true;
   }
   else if(this->Output < this->OutputMin)
   {
      this->SaturationOutput = this->OutputMin;
      this->OutputSaturated = true;
   }
   else
   {
      this->SaturationOutput = this->Output;
      this->OutputSaturated = false;
   }

   /* Do some more calculations for next iteration */
   if(   (this->OutputSaturated == true)
      && (this->Error * this->Output) > 0.0f) //if multiply is positive
   {
      this->IntegralLock = true;
   }
   else
   {
      this->IntegralLock = false;
   }

   return this->SaturationOutput;
}

void PID_SetOutputLimits(PID_T *this, float Max, float Min)
{
   this->OutputMax = Max;
   this->OutputMin = Min;
}
