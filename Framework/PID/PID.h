/*
 * PID.h
 *
 *  Created on: 25 wrz 2017
 *      Author: Lukasz
 */

#ifndef __PID_H_
#define __PID_H_

#include "stdbool.h"
#include "stdint.h"

typedef struct PID
{
   float IterationTime;

   float GainP;
   float GainI;
   float GainD;

   float OutputMax;
   float OutputMin;

   float Proportional;
   float Integral;
   float Derivative;

   float Error;
   float LastError;
   float IntegralLock;
   float Output;
   float SaturationOutput;
   bool OutputSaturated;
} PID_T;

void PID_Initialize(PID_T *this);
float PID_Process(PID_T *this, float DesiredValue, float ActualValue);

#endif /* __PID_H_ */
